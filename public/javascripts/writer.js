$(document).ready(function(){

	// user clicks on the edit of the news
	$('.addAdvertisementForm').on('submit',function(e){
		e.preventDefault();
		 $.ajax({
		 	url:'/writer',
		 	type:'post',
		 	dataType:'json',
		 	data:$('.addAdvertisementForm').serialize(),
		 	success:function(data){if(data.REPLY === '150'){
					location.href="/login";
				}else{
					location.reload();
					}},
		 	error:function(err){ alert('something went wrong');}

		 });
	});

	$('.editNewsClass').click(function(e){

		e.preventDefault();
		$('.addNewsDiv').hide('fast');
	 	$('.CategoryList').hide('fast');
	 	$('.editNewsDiv').show('fast');

	 	var index = $(this).attr('id');
	 	var category = advertisementData[index].category;
	 	var companyName = advertisementData[index].fullName;
	 	var picture = advertisementData[index].photoUrl;
	 	var publish = advertisementData[index].status;
	 	if(publish === true){
	 		publish = '1';
	 	}else{
	 		publish = '0';
	 	}
		//alert(category._id);
		
		for(var i=0;i<categoryData.length;i++){
			for(var j=0;j<category.length;j++){
				if (categoryData[i].name == category[j].name) {
				//code 
					console.log('id =' + category[j].name);
					$('.editContentForAdvertisementForm select[name=category] option[value=' +category[j]._id+']').prop('selected', true); 
				}	
			} 	
		}
	 	$('.editContentForAdvertisementForm select[name=status]').val(publish); 
	 	$('.editContentForAdvertisementForm input[name=fullName]').val(companyName);
	 	$('.editContentForAdvertisementForm input[name=photoUrl]').val(picture);
	 	$('.editContentForAdvertisementForm input[name=advertisementId]').val(advertisementData[index]._id);
	});


	$('.addAdvertisement').click(function(){
		 
	 	$('.addNewsDiv').show('fast');
	 	$('.CategoryList').hide('fast');
	 	$('.editNewsDiv').hide('fast');
	 	$('.editContact').hide('fast');
	 });
	 
	 $('.closeAddNewsDiv').click(function(){
	 	$('.addNewsDiv').hide('fast');
	 	$('.CategoryList').show('fast');
	 	$('.editNewsDiv').hide('fast');
	 	$('.editContact').hide('fast');
		$('.editContentForAdvertisementForm select[name=category] option:selected').prop('selected', false); 
	});


	$('.editContentForAdvertisementForm').on('submit', function(e){
		e.preventDefault();
		$.ajax({
			url:'/writer/editWriter',
			type:'post',
			data:$('.editContentForAdvertisementForm').serialize(),
			dataType:'json',
			success:function(data){ if(data.REPLY === '150'){
					location.href="/login";
				}else{
					location.reload();
					}},
			error:function(err){alert('something went wrong');}
		});
	});

	$('.editContactClass').click(function(e){
		e.preventDefault();
		$('.addNewsDiv').hide('fast');
	 	$('.CategoryList').hide('fast');
	 	$('.editNewsDiv').hide('fast');
	 	$('.editContact').show('fast');

	 	var index = $(this).attr('id');
	 	var name = advertisementData[index].contact.name;
	 	console.log(advertisementData[index].contact);
	 	var phone = advertisementData[index].contact.phone;
	 	var email = advertisementData[index].contact.email;
	 	var contactId = advertisementData[index].contact._id;
	 	$('.editContactForm input[name=name]').val(name);
	 	$('.editContactForm input[name=phone]').val(phone);
	 	$('.editContactForm input[name=email]').val(email);
	 	$('.editContactForm input[name=contactId]').val(contactId);
	});


	$('.editContactForm').on('submit', function(e){
		e.preventDefault();
		$.ajax({
			url:'/advertisement/editContact',
			type:'post',
			dataType:'json',
			data:$('.editContactForm').serialize(),
			success:function(data){
				if(data.REPLY === '150'){
					location.href="/login";
				}else{
					location.reload();
					}},
			error:function(err){alert('something went wrong');	}
		});
	});
});