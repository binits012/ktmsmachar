$(document).ready(function(){

	 $('.addCategoryForm').on('submit',function(e){
	 	e.preventDefault();
	 	$.ajax({
	 		url:'/news/addCategory',
	 		data:$('.addCategoryForm').serialize(),
	 		type:'post',
	 		dataType:'json',
	 		success:function(data){
	 			if(data.REPLY === '150'){
					location.href="/login";
				}else{
					location.reload();
					}
	 		},
	 		error:function(err){
	 			alert('something went wrong');
	 		}
	 	});
	 });

	 $('.editUserButtonClass').click(function(e){
	 	e.preventDefault();
	 	var index = $(this).attr('id');
	 	//alert(categoryData[index].publish);

	 	$('.editCategoryForm input[name=category]').val(categoryData[index].name);
	 	if(categoryData[index].publish === true)
	 		$('.editCategoryForm select[name=publish]').val('1');
	 	else
	 		$('.editCategoryForm select[name=publish]').val('0');
	 	$('.editCategoryForm input[name=index]').val(categoryData[index].index);
	 	$('.editCategoryForm input[name=categoryId]').val(categoryData[index]._id);

	 });

	 $('.editCategoryForm').on('submit',function(e){

	 	e.preventDefault();
	 	$.ajax({

	 		url:'/news/editCategory',
	 		type:'post',
	 		dataType:'json',
	 		data:$('.editCategoryForm').serialize(),
	 		success:function(data){
                            if(data.REPLY === '150'){
					location.href="/login";
				}else{
					location.reload();
					}},
	 		error:function(err){ alert('something went wrong. try later');}


	 	});

	 });


	 // submitting news
	 /*
	 $('.addContentForNewsForm').on('submit', function(e){
	 	e.preventDefault();

	 	var check = new FormData($(this)[0]);
	 	var data  = $(this).serialize();
	 	console.log(data);
	 	if(check){
	 		console.log() 
	 		$.ajax({
	 			url:'/news/addNews',
	 			type:'post',
	 			dataType:'json',
	 			async: false,
            	cache: false,
            	contentType: false,
           		processData: false,
	 			data:data,
	 			success:function(data){},
	 			error:function(err){alert('something went wrong');}
	 		})
	 	}
	 })
	 */

	 $('.addNewNews').click(function(){
	 	$('.addNewsDiv').show('fast');
	 	$('.CategoryList').hide('fast');
	 	$('.editNewsDiv').hide('fast');
	 });
	 
	 $('.closeAddNewsDiv').click(function(){
	 	$('.addNewsDiv').hide('fast');
	 	$('.CategoryList').show('fast');
	 	$('.editNewsDiv').hide('fast');
	 });

	 // submitting edit form


	 $('.editContentForNewsForm').on('submit', function(e){
	 	e.preventDefault();
	 	//alert( CKEDITOR.instances.editParagraph.getData());
	 	var para = CKEDITOR.instances.editParagraph.getData();
	 	$('.editContentForNewsForm textarea[name=editParagraph]').val(para);
		//CKEDITOR.instances.editParagraph.setData( para  );
		//alert(para);
		 
	 	$.ajax({
	 		url:'/news/editNews',
	 		type:'post',
	 		dataType:'json',
	 		data:$('.editContentForNewsForm').serialize(),
	 		success:function(data){
                            if(data.REPLY === '150'){
					location.href="/login";
				}else{
					location.reload();
					}},
	 		error:function(err){ alert('something broke. try again');}

	 	});
	 	 
	 });
	 
	 //comment
	 $('.editCommnetClass').click(function(e){
	 	e.preventDefault();
	 	$('.commentList').hide('fast');
	 	$('.editCommentDiv').show('fast');
	 	var id = $(this).attr('id');
	 	var comment = commentData[id].comment;
	 	$('.editComment textarea[name=comment]').val(comment);
	 	var publish = commentData[id].publish;
	 	if(publish === true){
	 		$('.editComment select[name=publish]').val('1');
	 	}else{
	 		$('.editComment select[name=publish]').val('0');
	 	}

	 	$('.editComment input[name=commentId]').val(commentData[id]._id);
	 });

	 $('.closeEditCommentDiv').click(function(){
	 	$('.commentList').show('fast');
	 	$('.editCommentDiv').hide('fast');
	 });


	 $('.editComment').on('submit',function(e){
	 	e.preventDefault();
	 	 
	 	$.ajax({
	 		url:'/news/updatePublishOrHideComment',
	 		type:'post',
	 		dataType:'json',
	 		data:$('.editComment').serialize(),
	 		success:function(data){ 
	 			if(data.REPLY === '150'){
					location.href="/login";
				}else{
					location.reload();
					}
	 		},
	 		error:function(err){alert('something went wrong try again');}
	 	});
	 	 
	 });

});

 //user clicks on next arrow on news section
	 $(document).on('click', '.arrowLeft',function(e){
	 	e.preventDefault();
	 	var pageNumber = $('input[name=arrowLeft]').val();
	 	if(pageNumber===''){
	 		pageNumber = 0;
	 	}else{
	 		pageNumber++;
	 		$('.arrowRight').show();
	 	}
	 	$('input[name=arrowLeft]').val(pageNumber);
	 	 

	 	 $.ajax({
	 	 	url:'/news/addNews?page='+pageNumber,
	 	 	dataType:'json',
	 	 	type:'get',
	 	 	success:function(data){ 
	 	 		if(data){
	 	 			//$('.recentlyAddedNewsListTable').remove();
	 	 			newsData = data;
	 	 			var table = ' <table class="table table-bordered table-hover table-striped recentlyAddedNewsListTable"> <thead> <tr> <th>Name </th>'+
	 	 			 '<th>Category</th>  <th>Created At  </th> <th>Action</th></tr> </thead>';
	 	 			 for(var i=0;i<data.length;i++){
	 	 			 	table += '<tr><td>'+data[i].newsTitle+'</td>'+
	 	 			 	'<td>' + data[i].newsCategory.name + '</td>'+
	 	 			 	'<td>' + data[i].createdAt.toString().slice(0,10) + '</td>' +
	 	 			 	'<td> <a href="#" data-target="#editNews" data-toggle="modal" id="'+i+'" class="editNewsClass">Edit</a></td></tr>'; 
	 	 			 }
	 	 			 table += '</table>';

     				$('.recentlyAddedNewsListTable').html(table)
	 	 		}
	 	 	},
	 	 	error:function(err){
	 	 		alert(err);
	 	 	}
	 	 });

	 });
	  $(document).on('click', '.arrowRight',function(e){
	 	e.preventDefault();
	 	var pageNumber = $('input[name=arrowLeft]').val();
	 	if(pageNumber==='' || pageNumber<=0){
	 		pageNumber = 0;
	 		$('.arrowRight').hide();
	 	}else{
	 		--pageNumber;
	 		if(pageNumber == 0){
	 			$('.arrowRight').hide();
	 		}
	 	}
	 	$('input[name=arrowLeft]').val(pageNumber);
	 	//alert(pageNumber);
	 	$.ajax({
	 	 	url:'/news/addNews?page='+pageNumber,
	 	 	dataType:'json',
	 	 	type:'get',
	 	 	success:function(data){ 
	 	 		if(data){
	 	 			//$('.recentlyAddedNewsListTable').remove();
	 	 			newsData = data;
	 	 			var table = ' <table class="table table-bordered table-hover table-striped recentlyAddedNewsListTable"> <thead> <tr> <th>Name </th>'+
	 	 			 '<th>Category</th>  <th>Created At  </th> <th>Action</th></tr> </thead>';
	 	 			 for(var i=0;i<data.length;i++){
	 	 			 	table += '<tr><td>'+data[i].newsTitle+'</td>'+
	 	 			 	'<td>' + data[i].newsCategory.name + '</td>'+
	 	 			 	'<td>' + data[i].createdAt.toString().slice(0,10) + '</td>' +
	 	 			 	'<td> <a href="#" data-target="#editNews" data-toggle="modal" id="'+i+'" class="editNewsClass">Edit</a></td></tr>'; 
	 	 			 }
	 	 			 table += '</table>';

     				$('.recentlyAddedNewsListTable').html(table)
	 	 		}
	 	 	},
	 	 	error:function(err){
	 	 		alert(err);
	 	 	}
	 	 });

	 });
// pagination ends here

// user clicks on the edit of the news
$(document).on('click','.editNewsClass', function(e){
	e.preventDefault();
	var id = $(this).attr('id');
        console.log(newsData);
	var categoryId = newsData[id].newsCategory._id;
	var newsTitle = newsData[id].newsTitle;
        console.log( newsData[id].newsContent[0]);
	var newsContent = newsData[id].newsContent[0].newsContent;
	var breaking = newsData[id].breaking;
        var priority = newsData[id].priority;
        var initialNews = newsData[id].initialNews;
        var initialPicture = newsData[id].initialPicture;
        var newsWriter = newsData[id].newsWriter;
        var newsWriterImg = newsData[id].newsWriterImg;
        
        breaking = (breaking === false)? $('.editContentForNewsForm select[name=breaking]').val('0'):$('.editContentForNewsForm select[name=breaking]').val('1');
        priority= (priority === false)? $('.editContentForNewsForm select[name=priority]').val('0'):$('.editContentForNewsForm select[name=priority]').val('1');
        var loactionName = newsData[id].locationName;
        
        $('.editContentForNewsForm select[name=category]').val(categoryId);
	$('.editContentForNewsForm input[name=newsTitle]').val(newsTitle);
	$('.editContentForNewsForm input[name=newsId]').val(newsData[id]._id);
        $('.editContentForNewsForm input[name=locationName]').val(loactionName);
        $('.editContentForNewsForm input[name=initialNews]').val(initialNews);
        $('.editContentForNewsForm input[name=initialPicture]').val(initialPicture);
        $('.editContentForNewsForm input[name=newsWriter]').val(newsWriter);
        $('.editContentForNewsForm input[name=newsWriterImg]').val(newsWriterImg);
	CKEDITOR.instances.editParagraph.setData( newsContent  );
	$('.CategoryList').hide('fast');
	$('.editNewsDiv').show('fast');
	$('.addNewsDiv').hide('fast');
        
        
        
        
        
        
        
        
        // writer change
        
        var categorySelection = $('select#editcategorySelection'); 
        var writerSelection = $('select#editWriterSelection');
        writerSelection.empty().append(function(){
         //first we empty everything from the selection of writer 
         var output = '<option > --Select Writer--</option>';
         
         for(var i = 0;i<writerData.length;i++){
          console.log(writerData[i].fullName);
          for(var j = 0; j<writerData[i].category.length; j++){
           if(categorySelection.val() === writerData[i].category[j]._id){
             console.log('its equal' + writerData[i].fullName);
            output += '<option value='+ writerData[i]._id  +'>' +writerData[i].fullName + '</option>';
           }
          } 
          
         }
         return output;
          
         });
         
        for(var i=0;i<writerData.length;i++){
              
               
              if (writerData[i]._id == newsData[id].writer._id ) {
                     //code
                     //alert('match');
                     $('.editContentForNewsForm select[name=writer]').val(writerData[i]._id);
              }
        }
        
        
        $('.editContentForNewsForm #editcategorySelection').change(function(e){
         //alert($(this).val());
         writerSelection.empty().append(function(){
         //first we empty everything from the selection of writer 
         var output = '<option > --Select Writer--</option>';
         
         for(var i = 0;i<writerData.length;i++){
          console.log(writerData[i].fullName);
          for(var j = 0; j<writerData[i].category.length; j++){
           if(categorySelection.val() === writerData[i].category[j]._id){
             console.log('its equal' + writerData[i].fullName);
            output += '<option value='+ writerData[i]._id  +'>' +writerData[i].fullName + '</option>';
           }
          } 
          
         }
         return output;
          
         });
        });

});



function addContentForNews(){
	 
	 
    var form = $('.addContentForNewsForm ' );

    var appendingContent = '<br><p>------------- New Section -----------------<br><div class="form-group"> <label for="memberName" class="col-sm-2 control-label">Content</label> '+
    						'<div class="col-sm-10"> <textarea rows="10" name="paragraph" required="required" class="form-control"></textarea>' + 
 							' </div> </div> <div class="form-group"> <label for="memberName" class="col-sm-2 control-label">Quotes</label> ' +
							' <div class="col-sm-10">  <textarea rows="3" name="quotes" required="required" class="form-control">  </textarea> </div> '+ 
							'</div> <div class="form-group"> <label for="memberName" class="col-sm-2 control-label">Photo</label> <div class="col-sm-10">' +
    						'<input type="file" name="pic" accept="image/*"/> </div> </div> <div class="form-group"> <label for="memberName" class="col-sm-2 control-label">Video Link</label>'+
  							'<div class="col-sm-10"> <input type="text" name="video" class="form-control"/> </div> </div>';
  	form.append(appendingContent);
  	return true;

}