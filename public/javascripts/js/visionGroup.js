/*!
 * 
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('.enrollmentFormDiv').hide('fast');
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        console.log('tere');
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
        
    });
    
    
    $('.postform').change(function(e){
        e.preventDefault();
        $(this).blur(); 
        var type = $(this).val();
        var searchBy = $('select.searchedBy');
        $('input[name=selectionType]').val(type);
         $('.searchedBy').show('fast');
        if (type === 'grade') {
             
            searchBy.empty().append(function(){
                //first we empty everything from the selection of teacher
                var searchByArray = [];
                var output = '<option > --Select Grade--</option>';
                 
                for(var i = 0;i<gradeData.length;i++){ 
                   output += '<option value='+ gradeData[i]._id  +'>' +gradeData[i].name + '</option>'; 
                 
                }
                
                return output;
                 
            }); 
             
             
        }else if (type === 'subject') {
            searchBy.empty().append(function(){
                //first we empty everything from the selection of teacher
                var searchByArray = [];
                var output = '<option > --Select Subject--</option>';
                 
                for(var i = 0;i<subjectData.length;i++){ 
                   output += '<option value='+ subjectData[i]._id  +'>' +subjectData[i].name + '</option>'; 
                 
                }
                
                return output;
                 
            }); 
        }else if (type === 'teacher') {
            searchBy.empty().append(function(){
                //first we empty everything from the selection of teacher
                var searchByArray = [];
                var output = '<option > --Select Teacher--</option>';
                 
                for(var i = 0;i<teacherData.length;i++){ 
                   output += '<option value='+ teacherData[i]._id  +'>' +teacherData[i].name + '</option>'; 
                 
                }
                
                return output;
                 
            }); 
        }else{
            // this is default behaviour ie show by date
            $('.searchedBy').hide('fast');
            
            var data={'id':'default','searchType':'default'}
            $.ajax({
            url:'/searchTuition',
            dataType:'json',
            data:data,
            type:'post',
            success:function(data){
                //alert('dfd');
                 
                var myList = '<div class= "myTuitionList">';
                if(data!=null){
                    console.log(data);
                    for (var i=0;i<data.length;i++) {
                        
                        var hour = Math.floor(data[i].classTime/60);
                        var min = data[i].classTime%60;
                        if(min < 10) min = '0'+min;
                        if(hour<10){hour = '0'+hour;} 
                        myList+='<div class = "col-sm-4 col-md-4 col-lg-3 col-xs-12" style="padding-top: 20px;"> ' + '<div class="thumbnail block"> <div class="tuitionDescription">' +
                        '<br />  <legend> ' + data[i].subject.name+ '</legend>'+
                        '<ul><li><strong> Start Date :</strong>'+' '+ data[i].startDate.toString().slice(0,10) +'</li>'+
                        '<li><strong> End Date :</strong>'+' '+ data[i].endDate.toString().slice(0,10) +'</li>'+
                        '<li><strong> Class Grade :</strong>'+' '+ data[i].grade.name +'</li>'+
                        '<li><strong> Class Time :</strong>'+' '+ hour+':'+min +'</li>'+
                        '<li><strong> Teacher :</strong>'+' '+ data[i].teacherName.name +'</li>'+
                        '<li><strong> Fee :</strong>'+' '+ data[i].tuitionFee +'</li>'+
                        '<li><strong> Seats Available :</strong>'+' '+ data[i].seatsAvailable +'</li>'+
                        '</ul>'+
                         '<br /><div class="serviceDescriiptionJoinButton text-center"><a href="#" target="ext" class="serviceDescription-btn enrolClassButton" id="'+i+'" >Enrol</a></div>'+
                        '</div></div></div></div>'; 
                                             
                    }
                    console.log(myList)
                    $('.tuitionList').show('fast');
                   $('.myTuitionList').html(myList); 
                }  
                
            },
            error:function(err){
                alert('something went wrong. Please try again');
            }
        });
        }
    });
    
    
    //user changed the searched criteria
    $('.searchedBy').change(function(e){
        e.preventDefault();
        $(this).blur();
        var searchType = $('input[name=selectionType]').val();
        console.log(searchType);
        var data = {
            'id':$(this).val(),
            'searchType':searchType
        }
        $('.tuitionList').hide('fast');
        //$('.myTuitionList').remove();
        $.ajax({
            url:'/searchTuition',
            dataType:'json',
            data:data,
            type:'post',
            success:function(data){
                //alert('dfd');
                 
                var myList = '<div class= "myTuitionList">';
                if(data!=null){
                    if (data.length >0) {
                        
                       for(var j=0; j< tuitionData.length; j++){
                        
                            var id = tuitionData[j]._id;
                            
                            for (var i=0;i<data.length;i++) {
                                if (id === data[i]._id) {
                                    var hour = Math.floor(data[i].classTime/60);
                                    var min = data[i].classTime%60;
                                    if(min < 10) min = '0'+min;
                                    if(hour<10){hour = '0'+hour;} 
                                    myList+='<div class = "col-sm-4 col-md-4 col-lg-3 col-xs-12" style="padding-top: 20px;"> ' + '<div class="thumbnail block"> <div class="tuitionDescription">' +
                                    '<br />  <legend> ' + data[i].subject.name+ '</legend>'+
                                    '<ul><li><strong> Start Date :</strong>'+' '+ data[i].startDate.toString().slice(0,10) +'</li>'+
                                    '<li><strong> End Date :</strong>'+' '+ data[i].endDate.toString().slice(0,10) +'</li>'+
                                    '<li><strong> Class Grade :</strong>'+' '+ data[i].grade.name +'</li>'+
                                    '<li><strong> Class Time :</strong>'+' '+ hour+':'+min +'</li>'+
                                    '<li><strong> Teacher :</strong>'+' '+ data[i].teacherName.name +'</li>'+
                                    '<li><strong> Fee :</strong>'+' '+ data[i].tuitionFee +'</li>'+
                                    '<li><strong> Seats Available :</strong>'+' '+ data[i].seatsAvailable +'</li>'+
                                    '</ul>'+
                                    '<br /><div class="serviceDescriiptionJoinButton text-center"><a href="#" target="ext" class="serviceDescription-btn enrolClassButton" id="'+j+'" >Enrol</a></div>'+
                                    '</div></div></div></div>';  
                                }
                               
                                                 
                            }
                        } 
                       
                    }else{
                        myList = ' <h4 style="color:red"> Sorry there are no Tuitions or Trainings available on the given search !! </h4>  '
                    }
                    
                    console.log(myList)
                    $('.tuitionList').show('fast');
                   $('.myTuitionList').html(myList); 
                }    
                
            },
            error:function(err){
                alert('something went wrong. Please try again');
            }
        });
    });
    
    
    //submitting the enrolment form
    $('.enrollmentForm').on('submit',function(e){
        e.preventDefault();
        $.ajax({
            url:'/enrolmentRequest',
            type:'post',
            dataType:'json',
            data:$('.enrollmentForm').serialize(),
            success:function(data){
               /* location.reload();*/
               if (data.REPLY === "ALREADY") {
                    alert('Thank you, we have already received your request')
               }else if (data.REPLY === "OK") {
                    alert('Thank you for your request.');
               }else if (data.REPLY ==="NOK") {
                    alert('Something went wrong. Please try again after a while.');
               }
               
               
                $('.tuitionList').show('fast');
                $('.enrollmentFormDiv').hide('fast');
                 
                $('.postform').show('fast');
                
                $('.enrollmentForm')[0].reset();
            },
            error:function(err){
                alert('Something went wrong.');
            }
        });
    });
    
});

// Highlight the top nav as scrolling occurs
$('body').scrollspy({
    target: '.navbar-fixed-top'
})

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});


// click on enrol button
$(document).on('click','.enrolClassButton',function(e){
    e.preventDefault();
    var index = $(this).attr('id');
    console.log('index =' + index);
    console.log(tuitionData[index]);
    $('.enrollmentForm input[name=startDate]').val(tuitionData[index].startDate.slice(0,10));
    $('.enrollmentForm input[name=endDate]').val(tuitionData[index].endDate.slice(0,10));
    $('.enrollmentForm input[name=teacherName]').val(tuitionData[index].teacherName.name);
    $('.enrollmentForm input[name=tuitionFee]').val(tuitionData[index].tuitionFee);
    var hour = Math.floor(tuitionData[index].classTime/60);
    var min = tuitionData[index].classTime%60;
    if(min < 10) min = '0'+min;
    if(hour<10){hour = '0'+hour;}
    $('.enrollmentForm input[name=classTime]').val( hour+':'+min);
    $('.enrollmentForm input[name=tuitionId]').val(tuitionData[index]._id);
    $('.enrollmentForm input[name=subjectName]').val(tuitionData[index].subject.name);
    $('.tuitionList').hide('fast');
    $('.enrollmentFormDiv').show('fast');
    $('.searchedBy').hide('fast');
    $('.postform').hide('fast');
    
});

$('.closeEnrolButton').click(function(e){
    e.preventDefault();
    $('.tuitionList').show('fast');
    $('.enrollmentFormDiv').hide('fast');
     
    $('.postform').show('fast'); 
})