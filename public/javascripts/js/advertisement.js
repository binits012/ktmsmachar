$(document).ready(function(){

	// user clicks on the edit of the news
	$('.addAdvertisementForm').on('submit',function(e){
		e.preventDefault();
		 $.ajax({
		 	url:'/advertisement',
		 	type:'post',
		 	dataType:'json',
		 	data:$('.addAdvertisementForm').serialize(),
		 	success:function(data){if(data.REPLY === '150'){
					location.href="/login";
				}else{
					location.reload();
					}},
		 	error:function(err){ alert('something went wrong');}

		 });
	});

	$('.editNewsClass').click(function(e){

		e.preventDefault();
		$('.addNewsDiv').hide('fast');
	 	$('.CategoryList').hide('fast');
	 	$('.editNewsDiv').show('fast');

	 	var index = $(this).attr('id');
	 	var category = advertisementData[index].category;
	 	var companyName = advertisementData[index].companyName;
	 	var picture = advertisementData[index].picture;
	 	var startAt = advertisementData[index].startAt.slice(0,10);
	 	var endAt = advertisementData[index].endAt.slice(0,10);
	 	var website = advertisementData[index].website;
	 	var position = advertisementData[index].position; 
	 	var publish = advertisementData[index].publish;
	 	if(publish === true){
	 		publish = '1';
	 	}else{
	 		publish = '0';
	 	}
		//alert(category._id);
	 	$('.editContentForAdvertisementForm select[name=category]').val(category._id);
	 	$('.editContentForAdvertisementForm select[name=publish]').val(publish);
	 	$('.editContentForAdvertisementForm input[name=companyName]').val(companyName);
	 	$('.editContentForAdvertisementForm input[name=picture]').val(picture);
	 	$('.editContentForAdvertisementForm input[name=startAt]').val(startAt);
	 	$('.editContentForAdvertisementForm input[name=endAt]').val(endAt);
	 	$('.editContentForAdvertisementForm input[name=website]').val(website);
	 	$('.editContentForAdvertisementForm input[name=position]').val(position);
	 	$('.editContentForAdvertisementForm input[name=advertisementId]').val(advertisementData[index]._id);
	});


	$('.addAdvertisement').click(function(){
		 
	 	$('.addNewsDiv').show('fast');
	 	$('.CategoryList').hide('fast');
	 	$('.editNewsDiv').hide('fast');
	 	$('.editContact').hide('fast');
	 });
	 
	 $('.closeAddNewsDiv').click(function(){
	 	$('.addNewsDiv').hide('fast');
	 	$('.CategoryList').show('fast');
	 	$('.editNewsDiv').hide('fast');
	 	$('.editContact').hide('fast');
	});


	$('.editContentForAdvertisementForm').on('submit', function(e){
		e.preventDefault();
		$.ajax({
			url:'/advertisement/editAdvert',
			type:'post',
			data:$('.editContentForAdvertisementForm').serialize(),
			dataType:'json',
			success:function(data){ if(data.REPLY === '150'){
					location.href="/login";
				}else{
					location.reload();
					}},
			error:function(err){alert('something went wrong');}
		});
	});

	$('.editContactClass').click(function(e){
		e.preventDefault();
		$('.addNewsDiv').hide('fast');
	 	$('.CategoryList').hide('fast');
	 	$('.editNewsDiv').hide('fast');
	 	$('.editContact').show('fast');

	 	var index = $(this).attr('id');
	 	var name = advertisementData[index].contact.name;
	 	console.log(advertisementData[index].contact);
	 	var phone = advertisementData[index].contact.phone;
	 	var email = advertisementData[index].contact.email;
	 	var contactId = advertisementData[index].contact._id;
	 	$('.editContactForm input[name=name]').val(name);
	 	$('.editContactForm input[name=phone]').val(phone);
	 	$('.editContactForm input[name=email]').val(email);
	 	$('.editContactForm input[name=contactId]').val(contactId);
	});


	$('.editContactForm').on('submit', function(e){
		e.preventDefault();
		$.ajax({
			url:'/advertisement/editContact',
			type:'post',
			dataType:'json',
			data:$('.editContactForm').serialize(),
			success:function(data){
				if(data.REPLY === '150'){
					location.href="/login";
				}else{
					location.reload();
					}},
			error:function(err){alert('something went wrong');	}
		});
	});
});