$(document).ready(function() {
 
 	$('.roleForm').on('submit',function (e) {
 		e.preventDefault();
 		$.ajax({
 			url:'/superUser/role',
 			dataType:'json',
 			data:$('.roleForm').serialize(),
 			type:'post',
 			success:function(data){
 				console.log(data);
 				if(data.REPLY === '100'){
 					alert('This role has already been added');
 				}else{
 					location.reload();
 				}
 				

 			},
 			error:function(err){
 				alert('केहि बिग्रेको अवस्था ')
 			}
 		});

 	});


 	$.validator.addMethod( "repasswordCheck",function(value, element, regexp) {

 		var pwd = $('.userForm input[name=userPassword]').val();
 		var repass = $('.userForm input[name=rePassword]').val();
 		if(pwd === repass){
 			return true;
 		}else{
 			return false;
 		}
		
        return this.optional(element) || regexp.test(value);
    },"");


 	$('.userForm').validate({
        rules:{
            rePassword:{repasswordCheck:true}
            
        },
        messages:{
            rePassword:{repasswordCheck:'Sorry, not the same password'},
            
        }
    });

 	$('.userForm').on('submit', function(e){
 		e.preventDefault();
 		var check = $('.userForm').valid();

 		if(check){
 			$.ajax({
	 			url:'/superUser/addUser',
	 			dataType:'json',
	 			data:$('.userForm').serialize(),
	 			type:'post',
	 			success:function(data){
	 				console.log(data);
	 				if(data.REPLY === '100'){
	 					alert('This role has already been added');
	 				}else{
	 					location.reload();
	 				}
	 				

	 			},
	 			error:function(err){
	 				alert('केहि बिग्रेको अवस्था , पुन प्रयाश गर्नु होला');
	 			}
	 		});
 		}else{
 			alert('केहि बिग्रेको अवस्था ');
 		}
 		
 	}); 


 	// adding user
 	$('.addUserButton').click(function(e){
 		$('.userList').hide('fast');
 		$('.roleList').hide('fast');
 		$('.createRoleDiv').hide('fast');
 		$('.createUserDiv').show('fast');
 		
 	});

 	$('.closeCreateUserFormDiv').click(function(){
 		$('.userList').show('fast');
 		$('.roleList').show('fast');
 		$('.createRoleDiv').hide('fast');
 		$('.createUserDiv').hide('fast');
 	});

 	$('.addRoleButton').click(function(e){
 		$('.userList').hide('fast');
 		$('.roleList').hide('fast');
 		$('.createRoleDiv').show('fast');
 		$('.createUserDiv').hide('fast');
 		
 	});

 	$('.closeCreateRoleFormDiv').click(function(){
 		$('.userList').show('fast');
 		$('.roleList').show('fast');
 		$('.createRoleDiv').hide('fast');
 		$('.createUserDiv').hide('fast');
 	});

 	
});