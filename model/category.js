(function(){

	var model = require('../model/mongoModel');

	var Category = (function(){
		function Category(name,publish,index){
			this.name = name;
			this.publish = publish;
			this.index = index;
		}
		Category.prototype.saveToDB = function(cb){
			var category = new model.Category({name:this.name, publish:this.publish,index:this.index});
			return category.save(function(err,data){
				if(!err){
					cb(data,null);
				}else{
					cb(null,err);
				}
			});
		}

		return Category;

	})();

	createCategory = function(name,publish,index,cb){

		var category = new Category(name,publish,index);
		return category.saveToDB(function(err,data){

			if(!err){
				cb(data,null);
			}else{
				cb(null,err);
			}
		});
	}

	getAllCategory = function(cb){
		return model.Category.find().sort({'index':1}).sort({'createdAt':-1}).exec(function(err,data){
			if(err){
				cb(err,null);
			}else{
				cb(data,null);
			}
		});
	}

	editCategory = function(id,name, publish, index, cb){
		return model.Category.findOneAndUpdate({'_id':id}, {$set:{name:name,publish:publish, index:index}},{new:true}, function(err,data){
			if(!err){
				cb(data, null);
			}else{
				cb(err,null);
			}

		})

	}

	getCategoryByName = function(name,cb){
		return model.Category.find({'name':name},{}, function(err,data){
			if(!err){
				cb(data,null);
			}else{
				cb(err, null);
			}
		})
	}
	root = typeof exports !== 'undefined' && exports !== null ? exports : window;
	root.Category = Category;
	root.createCategory = createCategory;
	root.getAllCategory = getAllCategory;
	root.editCategory = editCategory;
	root.getCategoryByName = getCategoryByName;

}).call(this);