(function(){
	var model = require('../model/mongoModel');

	var Location = (function(){
		function Location(placeName,loc,points){
			this.placeName = placeName;
			this.loc = loc;
			this.points = points;
			 

		}

	 
		Location.prototype.saveToDB = function(cb){
			var location = new model.Location({placeName:this.placeName,loc:this.loc, points:this.points });
			return location.save(function(err,data){
				if(!err){
					cb(data,null);
				}else{
					cb(null,err);
				}
			});
		}

		return Location;

	})();

	createLocation = function(placeName, lon, lat, points cb){

		var location = new Location(placeName,[lon,lat],points);
		return location.saveToDB(function(err,data){

			if(!err){
				cb(data,null);
			}else{
				cb(null,err);
			}
		});


	}

	root = typeof exports !== "undefined" && exports !== null ? exports : window;
	root.Location = Location;
	root.createLocation = createLocation;
}).call(this);