(function(){


	var model = require('../model/mongoModel');

	var CommentUser = (function(){


		function CommentUser(name,email){
			this.name = name;
			this.email = email;

		}

		CommentUser.prototype.saveToDB = function(cb) {
			// body..
			var commentUser = new model.CommentUser({name:this.name, email:this.email});

			return commentUser.save(function(err,data){
				if(!err){
					console.log('commentuser created');
					console.log(err);
					console.log(data);
					cb(data,null);
				}else{
					cb(null,err);
				}
			});
		};
		return CommentUser;
	})();

	createCommentUser = function(name,email,cb){
		var commentUser = new CommentUser(name,email);
		return commentUser.saveToDB(function(data,err){
			if(!err){
				cb(data,null);
			}else{
				cb(null,err);
			}
		})
	};

	searchUserByEmail = function(email,cb){

		return model.CommentUser.findOne({'email':email}).exec(function(err,data){
			if(!err){
				cb(data,null);
			}else{
				cb(null, err);
			}
		});
	}

	searchUserById = function(id,cb){
		return model.CommentUser.findById(id).exec(function(err,data){
			if(!err){
				cb(data,null);
			}else{
				cb(null, err);
			}
		});
	}

	root = typeof exports !== 'undefined' && exports !== null ? exports : window;
	root.CommentUser = CommentUser;
	root.createCommentUser = createCommentUser;
	root.searchUserByEmail = searchUserByEmail;
	root.searchUserById = searchUserById;


}).call(this);