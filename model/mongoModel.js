(function(){

	var mongoose = require('mongoose');
	var Schema = mongoose.Schema;
	var bcrypt = require('bcrypt');
	var SALT_WORK_FACTOR = 10;

	root = typeof exports !== "undefined" && exports !== null ? exports : window;
	//********** CATEGORY SCHEMA ********
	// ALL THE NEWS WILL BE LISTED VIA CATEGORY EG. HOME, SPORTS, ENTERTAINMENT ETC
	var categorySchema = new Schema({

		name:{type:String, unique:true},
		publish:Boolean,
		index:Number,
		createdAt:{type:Date, default:Date.now}
	});


	var locationSchema = new Schema({
		placeName:String,
		loc: {
		    type: [Number],  // [<longitude>, <latitude>]
		    index: '2d',      // create the geospatial index
	   
	    },
	    points:Number,
	    createdAt:{type:Date, default:Date.now}
	});
	var Location = mongoose.model('Location', locationSchema);
	root.Location = Location;

	var Category = mongoose.model('Category',categorySchema);
	root.Category = Category;


	var newsContentSchema = new Schema({
			newsContent:{type:String, unique:true},
			photos:String,
			videos:String,
			quotes:String
	});
	var NewsContent = mongoose.model('NewsContent', newsContentSchema );
	root.NewsContent = NewsContent;
	var newsSchema = new Schema({

		newsCategory:{type:mongoose.Schema.Types.ObjectId, ref:'Category'},
		newsTitle:{type:String, unique:true},
		newsContent:[{type:mongoose.Schema.Types.ObjectId, ref:'NewsContent'}],
		publish:{type:Boolean, default:true}, 
		location:{type:mongoose.Schema.Types.ObjectId, ref:'Location'},
		counter:{type:Number, default:1},
		comments:[{type:mongoose.Schema.Types.ObjectId, ref:'Comment'}],
		breaking:{type:Boolean, default:false},
		priority:{type:Boolean, default:false},
		
		writer:{type:mongoose.Schema.Types.ObjectId, ref:'Writer', default:null},
		locationName:{type:String, default:null},
		
		initialPicture:{type:String},
		initialNews:{type:String},
		newsWriter:{type:String},
		newsWriterImg:{type:String},
		
		createdAt:{type:Date, default:Date.now}

	});
	var News = mongoose.model('News',newsSchema);
	root.News = News;



//user role is defined here
	var roleSchema = new Schema({
		roleType:{type:String, unique:true},
		createdAt:{type:Date, default:Date.now}
	});

	var Role = mongoose.model('Role',roleSchema);
	root.Role = Role;


	var userSchema = new Schema({
		name:{type:String, required:true, unique:true},
		pwd:{type:String, required:true},
		role:{type:mongoose.Schema.Types.ObjectId, ref:'Role'},
		active:Boolean,
		createdAt:{type:Date, default:Date.now}

	});


	userSchema.pre('save', function(next){
		var user =  this;
		bcrypt.genSalt(SALT_WORK_FACTOR, function(err,salt){
			if(err){
				return next(err);
			}else{
				console.log(user  +' this is inside pre method \t'+ salt +user.pwd);
				bcrypt.hash( user.pwd, salt, function(err, hash){
					console.log('this is called');
					console.log(hash);
					if(err) return next(err);
					user.pwd = hash;
					next();
				});
			}
		});
	});
	

	userSchema.methods.comparePassword = function(pwd, cb){
		bcrypt.compare(pwd,this.pwd,function(err, match){
			if(err) return cb(err,null);
			cb(null,match);
		});
	}

	var User = mongoose.model('User', userSchema );
	root.User = User;


	//viewcounter of the site
	var viewcounterSchema = new Schema({
		viewCounterOfWhen:{type:String, unique:true},
		totalCounter:Number,
		pageCounter:{type:Number, default:1},

		createdAt:{type:Date, default:Date.now}

	});
	var ViewCounter = mongoose.model('ViewCounter',viewcounterSchema);
	root.ViewCounter = ViewCounter;
	
	//comment user

	var commentUserSchema = new Schema({
		name:{type:String, required:true},
		email:{type:String, required:true, unique:true},
		totalComments : {type:Number, default:0},
		createdAt:{type:Date, default:Date.now}
	});

	var CommentUser = mongoose.model('CommentUser', commentUserSchema );
	root.CommentUser = CommentUser;


// comment schema

	var commentSchema = new Schema({
	    comment: { type: String},
	    date: {type: Date, default: Date.now },
	    commentUser: {type:mongoose.Schema.Types.ObjectId, ref:'CommentUser'},
	    likesCount:{type:Number ,default:0},
	    dislikesCount:{type:Number, default:0},
	    replies:[{type:mongoose.Schema.Types.ObjectId, ref:'Comment'}],
	    publish:{type:Boolean, default:true}

  	});
  	var Comment = mongoose.model('Comment',commentSchema);
  	root.Comment = Comment;
	
	
	// contact schema
	var contactSchema = new Schema({
		name:{type:String, required:true},
		phone:{type:String, required:true},
		email:{type:String, required:true},
		createdAt:{type:Date, default:Date.now}
	});
	var Contact = mongoose.model('Contact', contactSchema);
	root.Contact = Contact;

	// advertisement Schema
	var advertSchema = new Schema({
		companyName:{type:String, required:true},
		picture:{type:String, required:true},
		startAt:{type:Date, required:true},
		endAt:{type:Date, required:true},
		publish:{type:Boolean, default:true},
		position:{type:Number, required:true},
		website:{type:String, required:true},
		category:{type:mongoose.Schema.Types.ObjectId, ref:'Category'},
		contact:{type:mongoose.Schema.Types.ObjectId, ref:'Contact'},
		createdAt:{type:Date, default:Date.now}
	});
	
	var Advertisement = mongoose.model('Advertisement', advertSchema);
	root.Advertisement = Advertisement;
	
	
	
	var writerSchema = new Schema({
		fullName:{type:String, required:true},
		photoUrl:String,
		contact:{type:mongoose.Schema.Types.ObjectId, ref:'Contact'},
		category:[{type:mongoose.Schema.Types.ObjectId, ref:'Category'}],
		status:{type:Boolean, default:true},
		createdAt:{type:Date, default:Date.now}
		
			
	});
	
	var Writer = mongoose.model('Writer', writerSchema);
	root.Writer = Writer;
	
	
	
	

}).call(this);