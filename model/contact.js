(function(){

	var model = require('../model/mongoModel');

	var root, createContact;
	var Contact = (function(){
  
		function Contact(name,phone, email){
			this.name = name;
			this.phone = phone;
			this.email = email;
		}

		Contact.prototype.saveToDB = function(cb) {
			// body...

			var contact = new model.Contact({name:this.name, phone:this.phone, email:this.email});

			return contact.save(function(err,data){
				if(!err){
					cb(data, null);
				}else{
					cb(null,err);
				}
			});
		};

		return Contact;
	})();

	createContact = function(name, phone, email, cb){
		var contact = new Contact(name, phone, email);
		console.log('contact data' + name + phone +email);
		return contact.saveToDB(function(data,err){

			console.log(data);
			if(!err){
				cb(data,null);
			}else{
				cb(null, err);
			} 
		});
	}

	updateContactById = function(id, name, phone, email, cb){
		return model.Contact.findByIdAndUpdate(id, {$set:{'name':name, 'phone':phone, 'email':email}}, {new:true},
			function(err,data){
				if(!err){
					cb(data,null);
				}else{
					cb(null, err);
				}
		});
	}

	root = typeof exports !== 'undefined' && exports !== null ? exports : window;
	root.Contact = Contact;
	root.createContact = createContact;
	root.updateContactById = updateContactById;


}).call(this);