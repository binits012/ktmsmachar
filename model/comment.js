(function(){
	var model = require('../model/mongoModel');
	var root;
	var Comment = (function(){ 
		function Comment(comment,commentUser){
			this.comment = comment;
			 
			this.commentUser = commentUser; 

		}
		Comment.prototype.saveToDB = function(cb) {
			// body...
			var comment = new model.Comment({comment:this.comment,  commentUser:this.commentUser });

			return comment.save(function(err,data){
				if(!err){
					cb(data, null);
				}else{
					cb(null,err);
				}
			});
		};
		return Comment;
	})();

	createComment = function(comment,  commentUser,cb){

		var comment = new Comment(comment, commentUser);

		return comment.saveToDB(function(data,err){
			if(!err){
				cb(data,null);
			}else{
				cb(null,err);
			}
		});
	}
 
 	updateLikeDislikeOfParticularComment = function(commentId, likeCounter, dislikeCounter, cb){

 		return model.Comment.findByIdAndUpdate(commentId,{$set:{'likesCount':likeCounter, 'dislikesCount':dislikeCounter}},{new:true},function(err,data){
 			console.log(err);
 			console.log(data);
 			if(!err){
 				cb(data,null);
 			}else{
 				cb(null,err);
 			}
 		});
 	}

 	getAllComments = function(cb){
 		return model.Comment.find().sort({'date':-1}).populate('commentUser').limit(100).exec(function(err,data){
 			if(!err){
 				cb(data,null);
 			}else{
 				cb(null,err);
 			}
 		});
 	}
 	updatePublishOrHideComment = function(id, publish, cb){
 		return model.Comment.findByIdAndUpdate(id,{$set:{'publish':publish}},{new:true},function(err,data){
 			console.log(err);
 			console.log(data);
 			if(!err){
 				cb(data,null);
 			}else{
 				cb(null,err);
 			}
 		});
 	}
	root = typeof exports !== 'undefined' && exports !== null ? exports: window;
	root.Comment = Comment;
	root.createComment = createComment;
	root.updateLikeDislikeOfParticularComment = updateLikeDislikeOfParticularComment;
	root.getAllComments = getAllComments;
	root.updatePublishOrHideComment = updatePublishOrHideComment;


}).call(this);