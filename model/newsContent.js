(function(){

	var model = require('../model/mongoModel');
	var NewsContent = (function(){

		/*
		newsContent:String,
			photos:String,
			videos:String,
			quotes:String
			*/
		function NewsContent(newsContent, photos, videos, quotes){
			this.newsContent = newsContent;
			this.photos = photos;
			this.videos = videos;
			this.quotes = quotes;

		}

		NewsContent.prototype.savaToDB = function(cb){
			var newsContent = new model.NewsContent({ newsContent:this.newsContent, photos:this.photos, videos:this.videos, quotes:this.quotes});
			return newsContent.save(function(err,data){
				if(!err){
					cb(data,null);
				}else{
					cb(err, null);
				}
			});
		}
		return NewsContent;
	})();

	createNewsContent = function(newsContent, photos, videos, quotes, cb){
		var newsContent = new NewsContent(newsContent, photos, videos, quotes);
		return newsContent.savaToDB(function(err,data){
			if(err){
				cb(err, null);
			}else{
				cb(data, null);
			}
		}); 
	}
	updateNewsContetById = function(id, newsContent, video, cb ){
		/* newsContent:{type:String, unique:true},
			photos:String,
			videos:String
		*/
		//console.log('newsContentId =' + id);
		return model.NewsContent.findOneAndUpdate({"_id":id},{$set:{newsContent:newsContent, videos:video}},{new:true},function(err,data){
			if(!err){
				//console.log('this is inside the newsContent -> update');
				//console.log(data);
				cb(data, null);
			}else{
				cb(err,null);
			}
		})
	}
	root = typeof exports !== 'undefined' && exports !== null ? exports:window;
	root.NewsContent = NewsContent;
	root.createNewsContent = createNewsContent;
	root.updateNewsContetById = updateNewsContetById;
}).call(this);