(function(){

	var model = require('../model/mongoModel');

	var Role = (function(){

		function Role(roleType){
			this.roleType = roleType;
		}

		Role.prototype.saveToDB = function  (cb) {
			 var role = new model.Role({roleType:this.roleType});
			 return role.save(function(err,data){
			 	
			 	if(err){
			 		console.log(err);
			 		cb(err,null);
			 	}else{
			 		cb(data,null);
			 	}
			 });
		}
		return Role;
	})();

	createRole = function(roleType,cb){
		var role = new Role(roleType);
		return role.saveToDB(function(err,data){
			if(err){
				cb(err,null);
			}else{
				cb(data,null);
			}
		});
	}

	getAllRole = function(cb){
		return  model.Role.find().exec(function(err,data){
			if(!err){
				cb(data,null);
			}else{
				cb(err,null);
			}
		});
	}

	findRoleById = function(id,cb){
		return model.Role.findById(id).exec(function(err,data){
			if(!err){
				cb(data,null);
			}else{
				console.log(err);
				cb(err, null);
			}
		});
	}

	root = typeof exports !== 'undefined' && exports !== null ? exports:window;
	root.Role = Role;
	root.createRole = createRole;
	root.getAllRole = getAllRole;
	root.findRoleById = findRoleById;
}).call(this);
