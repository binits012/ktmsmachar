(function(){

	var model = require('../model/mongoModel');

	var ViewCounter = (function(){

		function ViewCounter( viewCounterOfWhen,totalCounter){
			this.viewCounterOfWhen = viewCounterOfWhen;
			this.totalCounter = totalCounter;
			 
		}

		ViewCounter.prototype.saveToDB = function(cb) {
		 
			var viewCounter = new model.ViewCounter({viewCounterOfWhen:this.viewCounterOfWhen,totalCounter:this.totalCounter});
			return viewCounter.save(function(err,data){
				if(!err){
					cb(data,null);
				}else{
					cb(err,null);
				}
			});
		};
		return ViewCounter;
	})();

	createViewCounter = function(viewCounterOfWhen,number,cb){
		var viewCounter = new ViewCounter(viewCounterOfWhen,number);
		return viewCounter.saveToDB(function(data,err){
			if(!err){
				cb(data,null);
			}else{
				cb(null,err);
			}
		});
	}

	getTodayViewCounter = function(viewCounterOfWhen,cb){
		return model.ViewCounter.find({'viewCounterOfWhen':viewCounterOfWhen}).exec(function(err, data){
			if(!err){
				cb(data, null);
			}else{
				cb(null,err);
			}
		});

	}

	updateViewCounter = function(id,number, cb){
		//console.log(id+' number = ' + number);
		return model.ViewCounter.findByIdAndUpdate(id,{$set:{'pageCounter':number}},{new:true},function(err,data){
			if(!err){
				cb(data,null);
			}else{
				cb(null,err);
			}
		});
	}
	
	getLastSevenDaysViewCounter = function(cb){
		return model.ViewCounter.find().sort({createdAt:-1}).limit(7).exec(function(err, data){
			if(!err){
				cb(data, null);
			}else{
				cb(null,err);
			}
		});
	}
	
	getTotalViewCounterSoFar = function(cb){
		return model.ViewCounter.aggregate([{$group:{_id:null, pageVisit:{$sum:"$pageCounter"}}}], function(err, data){
			if(!err){
				cb(data, null);
			}else{
				cb(null, err);
			}
		});
	}
	root = typeof exports !== 'undefined' && exports !== null ? exports:window;
	root.ViewCounter= ViewCounter;
	root.createViewCounter = createViewCounter;
	root.getTodayViewCounter = getTodayViewCounter;
	root.updateViewCounter = updateViewCounter;
	root.getLastSevenDaysViewCounter = getLastSevenDaysViewCounter;
	root.getTotalViewCounterSoFar = getTotalViewCounterSoFar;
}).call(this);