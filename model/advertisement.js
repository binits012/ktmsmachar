(function(){
	var model = require('../model/mongoModel');

	var Advertisement = (function(){

		function Advertisement(companyName, picture, startAt, endAt, position, category, website, contact){
			this.companyName = companyName;
			this.picture = picture;
			this.startAt = startAt;
			this.endAt = endAt;
			//this.publish = publish;
			this.position = position;
			this.category = category;
			this.website = website;
			this.contact = contact;
		}

		Advertisement.prototype.saveToDB = function(cb) {
			// body...
			var advertisement = new model.Advertisement({companyName:this.companyName, picture:this.picture, startAt:this.startAt, endAt:this.endAt, 
				position:this.position, category:this.category, website:this.website, contact:this.contact});
			return advertisement.save(function(err,data){
				console.log(data);
				console.log('advertisement error');
				console.log(err);
				if(!err){
					cb(data, null);
				}else{
					cb(null, err);
				}
			});
		};

		return Advertisement;
	})();

	createAdvertisement = function(companyName, picture, startAt, endAt, position, category, website, contact,cb){
		var advertisement = new Advertisement(companyName, picture, startAt, endAt, position, category, website, contact);
		console.log('advertisement');
		console.log(companyName + picture + startAt + endAt + position + category + website + contact);
		return advertisement.saveToDB(function(data,err){
			if(!err){
				cb(data,null);
			}else{
				cb(null,err);
			}
		});
	};
	getAllAdvertisement = function(type, cb){
		if (type === 0) {
			return model.Advertisement.find().sort({'createdAt':-1}).populate('contact').populate('category').exec(function(err,data){
				if(!err){
					cb(data,null);
				}else{
					cb(null,err);
				}
			});
		}
		
		return model.Advertisement.find({'publish':true}).where({'endAt':{'$gt':new Date()}}).sort({'createdAt':-1}).populate('contact').populate('category').exec(function(err,data){
			if(!err){
				cb(data,null);
			}else{
				cb(null,err);
			}
		});
		
	}

 
	 	
	updateAdvertisementById = function(id, category, companyName, picture, startAt, endAt, website, position, publish,cb){
 
		return model.Advertisement.findByIdAndUpdate(id,{$set:{'companyName':companyName,'category':category, 'picture':picture, 
			'startAt':startAt, 'endAt':endAt, 'publish':publish, 'position':position, 'website':website}},{new:true}, 
			function(err,data){
				if(!err){
					cb(data,null);
				}else{
					cb(null, err);
				}
			});
	}
	
	getAdvertisementByCategory = function(categoryId, cb){
		return model.Advertisement.find({category:categoryId}).populate('contact').populate('category').exec(function(err,data){
			if(err){
				cb(null,err);	
			}else{
				cb(data,null);
			}	
		});
	}
	root = typeof exports !== 'undefined' && exports !== null ? exports : window;
	root.Advertisement = Advertisement;
	root.createAdvertisement = createAdvertisement;
	root.getAllAdvertisement = getAllAdvertisement;
	root.updateAdvertisementById = updateAdvertisementById;
	root.getAdvertisementByCategory = getAdvertisementByCategory;

}).call(this);