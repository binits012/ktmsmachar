(function(){
	var model = require('../model/mongoModel');
	var Category = require('../model/category');
	var NewsContent = require('../model/newsContent');
	var Comment = require('../model/comment');
	var logger = require("../model/logger");
	var News = (function(){

		function News(newsCategory, newsTitle, newsContent, publish, location,
		breaking,priority,writer,locationName, initialPicture, initialNews, newsWriter, newsWriterImg){
			/*
			newsCategory:{type:mongoose.Schema.Types.ObjectId, ref:'Category'},
		newsTitle:String,
		newsContent:[{type:mongoose.Schema.Types.ObjectId, ref:'NewsContent'}],
		publish:Boolean, 
		location:{type:mongoose.Schema.Types.ObjectId, ref:'Location'},
		initialPicture:{type:String},
		initialNews:{type:String},
		newsWriter:{type:String},
		newsWriterImg:{type:String},
		*/

			this.newsCategory = newsCategory;
			this.newsTitle = newsTitle;
			this.newsContent = newsContent;
			this.publish = publish;
			this.location = location;
			this.breaking = breaking;
			this.priority = priority;
			this.writer = writer;
			this.locationName = locationName;
			this.initialNews = initialNews;
			this.initialPicture = initialPicture;
			this.newsWriter = newsWriter;
			this.newsWriterImg = newsWriterImg;

		}

		News.prototype.saveToDB = function(cb){

			var news = new model.News({newsCategory:this.newsCategory, newsTitle:this.newsTitle, newsContent:this.newsContent, publish:this.publish,
			location:this.location, priority:this.priority, breaking:this.breaking,writer:this.writer,
			locationName:this.locationName, initialPicture:this.initialPicture, initialNews:this.initialNews,
			newsWriter:this.newsWriter, newsWriterImg:this.newsWriterImg});
			return news.save(function(err,data){
				if(!err){
					cb(data, null);
				}else{
					cb(err, null);
				}
			});
		}

		return News;
	})();

	createNews = function(newsCategory, newsTitle, newsContent, publish, location, breaking, priority, writer, locationName,initialPicture, initialNews, newsWriter, newsWriterImg, cb){
		var news = new News(newsCategory, newsTitle, newsContent, publish, location,breaking, priority,writer, locationName,initialPicture, initialNews, newsWriter, newsWriterImg);
		return news.saveToDB(function(err,data){
			if(!err){
				cb(data, null);
			}else{
				cb(err, null);
			}
		});
	}

// this is for getting the 10 recently added news
	getAllNews = function(cb){
		return model.News.find({'publish':true}).sort({'createdAt':-1}).populate('writer').populate('newsCategory').populate({path:'newsContent',  model:'NewsContent'}).limit(10).exec(function(err,data){
			if(!err){
				cb(data, null);
			}else{
				cb(err,null);
			}
		});
	}
// this is for getting 10 breaking news

	getBreakingNews = function(cb){
		return model.News.find({'publish':true,'breaking':true}).sort({'createdAt':-1}).populate('writer').populate('newsCategory').populate({path:'newsContent',  model:'NewsContent'}).limit(10).exec(function(err,data){
			if(!err){
				cb(data, null);
			}else{
				cb(err,null);
			}
		});
	}
// this is for getting 10 breaking news

	getPriorityNews = function(cb){
		return model.News.find({'publish':true,'priority':true}).sort({'createdAt':-1}).populate('writer').populate('newsCategory').populate({path:'newsContent',  model:'NewsContent'}).limit(10).exec(function(err,data){
			if(!err){
				cb(data, null);
			}else{
				cb(err,null);
			}
		});
	}
// this is for getting recently added news for admin purposes
	getRecentNews = function(page,cb){
		return model.News.find({}).sort({'createdAt':-1}).populate('writer').populate('newsCategory').populate({path:'newsContent',  model:'NewsContent'}).skip(page*10).limit(10).exec(function(err,data){
			if(!err){
				cb(data, null);
			}else{
				cb(err,null);
			}
		});
	}	

	getNewsOfSelection = function(id,cb){
		return model.News.find({'_id':id}).populate('newsCategory').populate('writer').populate({path:'newsContent',  model:'NewsContent'}).populate({path:'comments',
			'model':'Comment',options:{sort:{'date':-1}}, populate:{path:'commentUser', model:'CommentUser'}}).exec(function(err,data){
			
			if(!err && (typeof data != 'undefined')){
				var locationName = data[0].locationName;
				
				var writerId = null;
				if(!data[0].writer){
				}else{
					writerId = data[0].writer._id;

				}
				//var writerId = data[0].writer._id;
				
				if (writerId && locationName) {
					//code
					model.News.find({'locationName':locationName}).populate('newsCategory').populate('writer').populate({path:'newsContent',  model:'NewsContent'}).populate({path:'comments',
					'model':'Comment',options:{sort:{'date':-1}}, populate:{path:'commentUser', model:'CommentUser'}}).sort({'createdAt':-1}).sort({'counter':1}).limit(10).exec(function(err,doc){
						if (!err) {
							//code
							
							model.News.find({writer:writerId}).populate('newsCategory').populate('writer').populate({path:'newsContent',  model:'NewsContent'}).populate({path:'comments',
							'model':'Comment',options:{sort:{'date':-1}}, populate:{path:'commentUser', model:'CommentUser'}}).sort({'createdAt':-1}).sort({'counter':1}).limit(10).exec(function(err,writerData){
								if (!err) {
									 
									cb(data,doc,writerData, null); 
								}
							}); 
						}
					});
				}else if (writerId && !locationName) {
					//////console.log('no location data');
					model.News.find({writer:writerId}).populate('newsCategory').populate('writer').populate({path:'newsContent',  model:'NewsContent'}).populate({path:'comments',
							'model':'Comment',options:{sort:{'date':-1}}, populate:{path:'commentUser', model:'CommentUser'}}).sort({'createdAt':-1}).sort({'counter':1}).limit(10).exec(function(err,writerData){
								if (!err) {
									////console.log(writerData); 
									cb(data,null,writerData, null); 
								}
							}); 
					 
				}else if (!writerId && locationName) {
					 
					model.News.find({'locationName':locationName}).populate('newsCategory').populate('writer').populate({path:'newsContent',  model:'NewsContent'}).populate({path:'comments',
					'model':'Comment',options:{sort:{'date':-1}}, populate:{path:'commentUser', model:'CommentUser'}}).exec(function(err,doc){
						if (!err) {
							//code
							cb(data,doc,null, null);
						}
					});
				}else if(!writerId && !locationName){
					cb(data,null,null, null);
				}
				 
				
			}else{
				////console.log(new Date())
				////console.log(data);
				console.log(err)
				logger.error(err);
				cb(err,null,null,null);
			}
		});

	}

	//get news by category selection
	getNewsByCategorySelection = function(name,pageNumber, cb){
		if (pageNumber) {
			return Category.getCategoryByName(name, function(data,err){
				//////console.log('search by category');
				//////console.log(data[0].id);
				if(!err){
					return model.News.find({'newsCategory':data[0].id}).populate('writer').populate('newsCategory').populate({path:'newsContent',  model:'NewsContent'}).sort({createdAt:-1}).skip(pageNumber*10).limit(10).exec(function(err,data){
						//////console.log('search by category');
						//////console.log(data);
						if(err){
							cb(err, null);
						}else{
							cb(data, null);
						}
					});
				}else{
					cb(err, null);
				}
			});
		}else{
			return Category.getCategoryByName(name, function(data,err){
				//////console.log('search by category');
				//////console.log(data[0].id);
				if(!err){
					if (data.length>0) {
						//////console.log(data);
						return model.News.find({'newsCategory':data[0].id}).populate('writer').populate('newsCategory').populate({path:'newsContent',  model:'NewsContent'}).sort({createdAt:-1}).limit(10).exec(function(err,data){
							//////console.log('search by category');
							//////console.log(data);
							if(err){
								cb(err, null);
							}else{
								cb(data, null);
							}
						});
					}else{
						////console.log('what the error');
						////console.log(err);
						cb(null, err);
					}
					
				}else{
					cb(err, null);
				}
			});
		}
		
		
		
	}
	
	//get news by category -> highest counter ascending order
	getNewsByCategoryByHigestCounter = function(name, cb){
		return Category.getCategoryByName(name, function(data,err){
			//////console.log('search by category');
			//////console.log(data[0].id);
			if(!err){
				return model.News.find({'newsCategory':data[0].id}).populate('writer').populate('newsCategory').populate({path:'newsContent',  model:'NewsContent'}).sort({counter:-1}).limit(10).exec(function(err,data){
					//////console.log('search by category');
					//////console.log(data);
					if(err){
						cb(err, null);
					}else{
						cb(data, null);
					}
				});
			}else{
				cb(err, null);
			}
		});
		
	}

	getFirstTenNewsByCategorySelection = function(name, cb){
		return Category.getCategoryByName(name, function(data,err){
			//////console.log('search by category');
			//////console.log(data[0].id);
			if(!err){
				return model.News.find({'newsCategory':data[0].id}).populate('writer').populate('newsCategory').populate({path:'newsContent',  model:'NewsContent'}).limit(20).sort({createdAt:-1}).lean().exec(function(err,data){
					//////console.log('search by category');
					//////console.log(data);
					if(err){
						cb(err, null);
					}else{
						cb(data, null);
					}
				});
			}else{
				cb(err, null);
			}
		});
		
	}

	
	
	getNewsByPage = function( page,lower, cb){
		 
		console.log("inside the model");
		console.log(page*10);
		console.log(lower*10)
		/*
		return model.News.find( ).populate('writer').populate('newsCategory').populate({path:'newsContent',  model:'NewsContent'}).limit(page*10).sort({createdAt:-1}).lean().skip(10).exec(function(err,data){
			//////console.log('search by category');
			//////console.log(data);
			if(err){
				cb(err, null);
			}else{
				cb(data, null);
			}
		});
		*/
		return model.News.find({}).sort({'createdAt':-1}).populate('writer').populate('newsCategory').populate({path:'newsContent',  model:'NewsContent'}).skip(lower*10).limit(10).exec(function(err,data){
			if(!err){
				cb(data, null);
			}else{
				cb(err,null);
			}
		}); 
		 
		
	}


	

	// here we process the request of editing news
	editTheSelectedNews = function(category,newsTitle, paragraph, video , newsId, breaking, priority,writer, locationName, initialPicture, initialNews, newsWriter, newsWriterImg, cb){

			return model.News.findOne({'_id':newsId},{}, function(err,data){
				if(!err){
					//cb(data,null);
					//
					//////console.log('news content');
					//////console.log(data.newsContent[0]);
					return NewsContent.updateNewsContetById(data.newsContent[0], paragraph, video, function(doc,err){
						if(!err){
							data.newsContent = doc;
							//////console.log('updated news content');
							//////console.log(doc);
							data.newsTitle = newsTitle;
							data.newsCategory = category;
							data.breaking = breaking;
							data.priority = priority;
							data.writer = writer;
							data.locationName = locationName;
							data.initialPicture = initialPicture;
							data.initialNews = initialNews;
							data.newsWriter = newsWriter;
							data.newsWriterImg = newsWriterImg;
							data.save(function(err){
								if(!err){
									cb(data,null);
								}else{
									cb(err,null);
								}
							});


							
						}else{
							cb(err,null); 
						}
					});

				}else{
					cb(err,null);
				}
			});
	}
	//update the counter value of the News
	updateTheNewsCounter = function(id,cb){
		return model.News.findById(id,{}, function(err,data){
			if(!err){

				//////console.log('update counter');
				//////console.log(data);
				if (data) {
					var counter = data.counter + 1;
					data.counter = counter;
	
					data.save(function(err){
						if(err){
							cb(data,null);
						}else{
							cb(null,err);
						}
					});
				}else{
					cb(null, err);
				}
				

			}else{
				cb(null, err);
			}

		});

	}
	
	// we update the news as there is a comment coming for the news
	addNewComment = function(id,comment,cb){
		return model.News.findByIdAndUpdate(id, {$push:{'comments':comment}},function(err,data){
			if(!err){
				cb(data,null);
			}else{
				cb(null,err);
			}

		} );
	} 

	updateLikeDislikeCounterOfParticularComment = function(newsId,commentId,likeCounter,dislikeCounter,cb){
		return model.News.findById(newsId).exec(function(err,data){

			if(!err){
				var myData = data;
				return  Comment.updateLikeDislikeOfParticularComment(commentId, likeCounter, dislikeCounter, function(doc,err){
					if(!err){
						//////console.log('******updated comment ******');
						//////console.log(doc);
						 
						cb(myData,doc,null);
					}else{
						cb(null,null,err);
					}
				});
				
			}else{
				cb(null,err);
			}
		});
	}
	function getLastWeek(){
		var today = new Date();
		var lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);
		return lastWeek ;
	}
	getPopularNewsOfTheWeek = function(cb){
		var lastWeek = getLastWeek();
		var lastWeekMonth = lastWeek.getMonth() + 1;
		var lastWeekDay = lastWeek.getDate();
		var lastWeekYear = lastWeek.getFullYear();
		////console.log(lastWeekYear + ' ' + lastWeekMonth + ' ' +  lastWeekDay);
		var date = new Date(lastWeekYear,lastWeekMonth-1,lastWeekDay);
		////console.log(date);
		return model.News.find({'createdAt':{"$gte":new Date(date), "$lt":Date.now()}}).populate('writer').populate('newsCategory').populate({path:'newsContent',  model:'NewsContent'}).sort({counter:-1}).limit(20).exec(function(err,data){
			//////console.log('search by category');
			////console.log(data);
			if(err){
				cb(err, null);
			}else{
				cb(data, null);
			}
		});
			 
		 
	};
	
	root = typeof exports !== 'undefined' && exports !== null ? exports:window;
	root.News = News;
	root.createNews = createNews;
	root.getAllNews = getAllNews;
	root.getBreakingNews = getBreakingNews;
	root.getPriorityNews = getPriorityNews;
	root.getNewsOfSelection = getNewsOfSelection;
	root.getNewsByCategorySelection = getNewsByCategorySelection;
	root.getFirstTenNewsByCategorySelection = getFirstTenNewsByCategorySelection;
	root.getRecentNews = getRecentNews;
	root.editTheSelectedNews = editTheSelectedNews;
	root.getNewsByCategoryByHigestCounter = getNewsByCategoryByHigestCounter;
	root.updateTheNewsCounter = updateTheNewsCounter;
	root.addNewComment = addNewComment;
	root.updateLikeDislikeCounterOfParticularComment = updateLikeDislikeCounterOfParticularComment;
	root.getPopularNewsOfTheWeek = getPopularNewsOfTheWeek;
	root.getNewsByPage = getNewsByPage;

}).call(this);
