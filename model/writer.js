(function(){

    var model = require('../model/mongoModel');
    
    var Writer = (function(){
        
        function Writer(fullName,photoUrl, contact, category) {
            this.fullName = fullName;
	    this.photoUrl = photoUrl;
	    this.contact = contact;
	    this.category = category;
		
        }
        
        
        Writer.prototype.saveToDB = function(cb){
            var writer = new model.Writer({fullName:this.fullName,photoUrl:this.photoUrl, contact:this.contact, category:this.category});
            
            return writer.save(function(err,data){
                if(!err){
                    cb(data,null);    
                }else{
                    cb(null,err);    
                }    
            });
        }
        return Writer;
    })();
    
    createWriter = function(fullName,photoUrl, contact, category,cb){
        var writer = new Writer(fullName,photoUrl, contact, category);
        return writer.saveToDB(function(data,err){
                if(!err){
                        cb(data, null);
                }else{
                        cb(err, null);
                }
        });
    }
    
    getAllWriters = function(cb){
        return model.Writer.find().populate('contact').populate('category').sort({'createdAt':-1}).exec(function(err,data){
            if(!err){
                cb(data,null);
            }else{
                cb(null,err);    
            } 
        });
    }
    
    getAllActiveWriters = function(cb){
        return model.Writer.find({'status':true}).populate('contact').populate('category').sort({'createdAt':-1}).exec(function(err,data){
            if(!err){
                cb(data,null);
            }else{
                cb(null,err);    
            } 
        });
    }
    
    updateWriterById = function(id, category, companyName, picture, publish, cb){
	return model.Writer.findByIdAndUpdate(id,{'$set':{'fullName':companyName, 'photoUrl':picture, 'status':publish,
	    'category':category}},{new:true},function(err,data){
		if(!err){
		    cb(data,null);   
		}else{
		    cb(null,err);    
		}	
	    });
    }
    root = typeof exports !== 'undefined' && exports !== null ? exports:window;
    root.Writer = Writer;
    root.createWriter = createWriter;
    root.getAllWriters = getAllWriters;
    root.updateWriterById = updateWriterById;
    root.getAllActiveWriters = getAllActiveWriters;
    
    
}).call(this);