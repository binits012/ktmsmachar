(function(){
	var model = require('../model/mongoModel');
	var bcrypt = require('bcrypt');
	var SALT_WORK_FACTOR = 10;
	var User = (function(){
		function User(name, pwd, role,active){
			this.name = name;
			this.pwd = pwd;
			this.role = role;
			if(active==='1'){
				active = true;
			}else{
				active = false;
			}
			this.active = active;
		}

		User.prototype.saveToDB = function(cb){
			 var user = new model.User({
			 	name:this.name,
			 	pwd:this.pwd,
			 	role:this.role,
			 	active:this.active
			 });

			 return user.save(function(err,data){
			 	if(err){
			 		cb(err,null);
			 	}else{
			 		cb(data,null);
			 	}
			 });
			 
			 
		}
		return User;

	})();

	createUser = function(name,pwd,role,active,cb){
		var user = new User(name,pwd,role,active);
		console.log(user);
		console.log('*******');
		 
		return user.saveToDB(function(err,data){
			if(err){
				console.log(err);
				cb(err,null);
			}
			else {
				cb(data, null);
			}
		});
		 
	}

	loginCheck = function( name,pass, cb){
		return model.User.findOne({name:name}).exec(function(err,data){

			if(!err){
				console.log(data);
				if(data){
					data.comparePassword(pass,function(err, isMatch){
						if(isMatch){
							cb(data,null);
						}else{
							cb(err,null);
						}
					});
				}else{
					cb(err,null);
				}
				
			}else{

			}
			
		});
	}

	getAllUsers = function(cb){
		return model.User.find().populate('role').sort({createdAt:-1}).exec(function(err,data){
			if(!err){
				cb(data,null);
			}else{
				cb(err,null);
			}
		})
	}

	root = typeof exports !== "undefined" && exports !== null ? exports : window;
	root.User = User;
	root.createUser = createUser;
	root.loginCheck = loginCheck;
	root.getAllUsers = getAllUsers;

}).call(this);