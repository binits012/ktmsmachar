var nodemailer = require("nodemailer");
var fs = require('fs');
var transport = nodemailer.createTransport({
service: 'Gmail',
auth: {
    user: 'newsenepal2017@gmail.com',
    pass: 'newsenepal123'
}
});

var root = typeof exports !== "undefined" && exports !== null ? exports : window;

var SendMail = function(userName, toEmail, newsTitle, yourComment, likeCounter, dislikeCounter){
		this.toEmail=toEmail;
		this.userName = userName;
		this.newsTitle = newsTitle;
		this.yourComment = yourComment;
		this.likeCounter = likeCounter;
		this.dislikeCounter = dislikeCounter;
}

SendMail.prototype.sendMail = function(title,templateName){

	var user = this.userName;
	var email = this.toEmail;
	var newsTitle = this.newsTitle;
	var yourComment = this.yourComment;
	var likeCounter = this.likeCounter;
	var dislikeCounter = this.dislikeCounter;
	var fileLoaction = "";
    if (templateName === "firstComment") {
        fileLoaction = '/home/NewseNepal1/NewseNepal/public/templates/commentThankYou.html';
    }else if (templateName === "liked" || templateName === "disliked") {
        fileLoaction = '/home/NewseNepal1/NewseNepal/public/templates/likeDislike.html';
    }else if(templateName === "advertisementAdded"){
	fileLoaction = '/home/NewseNepal1/NewseNepal/public/templates/advertisementAdded.html';	
    } 
    fs.readFile(fileLoaction, function read(err, data) {
        if (err) {
            throw err;
        }

        var tempData = data.toString();
        tempData = tempData.replace('$user',  user);
        tempData = tempData.replace('$newsTitle',  newsTitle);
        tempData = tempData.replace('$userComment',  yourComment);
        tempData = tempData.replace('$likeCounter', likeCounter);
        tempData = tempData.replace('$dislikeCounter', dislikeCounter);
	
	
	//advertisement
	tempData = tempData.replace('$companyName', newsTitle); 
	tempData = tempData.replace('$startDate',yourComment );
	tempData = tempData.replace('$endDate', likeCounter);


          var message = { 
            // sender info
            from: 'newsenepal2017@gmail.com', 
            // Comma separated list of recipients
            to:  email, 
            // Subject of the message
            subject: title+' ' , // 
            html: tempData.toString()
        };
        console.log('************* email message ******************'); 
        console.log(message);

        transport.sendMail(message, function(error){
            if(!error){
                    console.log("Email has been sent"); 
                     
            }else{
                    console.log(error );
            
            }
        });
    
    }); 

}

module.exports = SendMail; 