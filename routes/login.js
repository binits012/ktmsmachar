var express = require('express');
var router = express.Router();
var Role = require('../model/role');
var User = require('../model/users');
var formidable = require('formidable');
var logger = require("../model/logger");  
/* GET home page. */
router.get('/', function(req, res) {
  res.render('login', { title: 'Login' });
});

router.post('/login', function(req, res){
	var username = req.body.username;
	var password = req.body.password;

	var form = new formidable.IncomingForm();
	 
	form.parse(req);
	form.on('field', function (field, value) {
		//console.log('fields');
        //console.log(field + " " + value);
         if(field === 'username'){
         	username = value;
         }else if(field === 'password'){
         	password = value;
         }
    });
    form.on('end', function() {
    	User.loginCheck(username, password,function(data,err){
			if(!err){
				//console.log(data);
				//res.send(data);
				if(data){
					Role.findRoleById(data.role,function(doc,err){
						if(!err){
							//console.log('ROLE');
							//console.log(doc.roleType);
							req.session.name = { "name" : data.name  };
							req.session.role = {"role":doc.roleType};
							logger.info('logged in user = ' + data.name  + ' Role ' + doc.roleType); 
							//res.send({'REPLY':doc.roleType});
							if(doc.roleType === 'superAdmin'){
								//console.log('superAdmin view');
						    	 res.redirect('/superUser');
						    }else if(doc.roleType === 'editor'){
						    	 res.redirect('/otherUser');
						    	 
						    }else if(doc.roleType === 'chiefEditor'){
						    	 res.redirect('/editorUser');
						    }else if(doc.roleType === 'typist'){
						    	 
						    }
						}else{
						      logger.info('login faied for  ' + username  ); 
							res.send({'REPLY': 'NOK'});
						}
					});
				}else{
					res.render('error',{title:'Error'});
				}
				
			}else{
				//console.log(err);
				//res.send(err);
				res.send({'REPLY': 'NOK'})
			}
		});
    });

	
});


//logged out
router.get('/out',function(req,res){

	if (req.session.name) {
	  //code
	  logger.info('logged out user  ' + req.session.name.name  ); 
	}
	
	req.session.name = null;
	req.session.role = null;
	
	res.redirect('/login');
});
module.exports = router;