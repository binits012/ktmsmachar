
var express = require('express');
var router = express.Router();
var Contact = require('../model/contact');
var Category = require('../model/category');
var Writer = require('../model/writer');
var formidable = require('formidable');
var SendMail = require('../model/sendMail');
var logger = require("../model/logger");

router.get('/',function(req, res){

 if(req.session.name){
	Category.getAllCategory(function(data,err){
			if(!err){
		              
                    Writer.getAllWriters(function(doc,err){
                        if(!err){
                            res.render('writer', { title: 'KTM Samachar', categoryData:data, advertisementData:doc });
                        }else{
                            res.redirect('/login');
                        }
                    });
        			
			}else{
				// need to handle the error
			}
	});
        
        }else{
		res.redirect('/login');
	}
});

router.post('/', function(req,res){

	if(req.session.name){
        var form = new formidable.IncomingForm();
	var category, companyName, picture, startAt, endAt, website, position, contactName, contactPhone, email;


	form.parse(req);

	form.on('field', function (field, value) {
		//console.log('fields');
        console.log(field + " " + value);
         if(field === 'category'){
         	category = value ;
         }else if(field === 'photoUrl'){
         	picture = value;
         } 
        
         else if(field === 'fullName'){
         	contactName = value;
         } 
         else if(field === 'phone'){
         	contactPhone = value;
         } 
         else if(field === 'email'){
         	email = value;
         } 
    });

    form.on('end',function(){

    	Contact.createContact(contactName,contactPhone,email, function(data,err){
    		if(!err){
    			console.log('contact front end');
    			//console.log(data);
                         
    			Writer.createWriter(contactName,picture, data._id, category, function(doc,err){
    				if(!err){
                                       // logger.info('new writer ' + name + ' created by user '+ req.session.name.name);    
    					res.send(doc);
    				}else{

    				}
    			});
                         
    		}else{
                     // this means either contact information is not provided or something went wrong
                     if (data === null) {
                           Writer.createWriter(contactName,picture, null, category, function(doc,err){
    				if(!err){
                                       // logger.info('new writer ' + name + ' created by user '+ req.session.name.name);    
    					res.send(doc);
    				}else{

    				}
    			});  
                     }
                     
    		}
    	});

    });
    
    }else{
               res.send({'REPLY':'150'});
		//res.redirect('/login');
	}


});

router.post('/editWriter',function(req,res){
if(req.session.name){
    var form = new formidable.IncomingForm();
    var category, companyName, picture, startAt, endAt, website, position, advertisementId, publish;


    form.parse(req);

    form.on('field', function (field, value) {
        //console.log('fields');
        //console.log(field + " " + value);
         if(field === 'category'){
            category = value ;
         }else if(field === 'fullName'){
            companyName = value;
         }else if(field === 'photoUrl'){
            picture = value;
         } 
         else if(field === 'advertisementId'){
            advertisementId = value;
         }else if(field === 'status'){
            publish = value;
         }
          
    });


    form.on('end', function(){
        startAt = new Date(startAt);
        endAt = new Date(endAt);
        if(publish === '1'){
            publish = true;
        }else{
            publish = false;
        }
        
        Writer.updateWriterById(advertisementId, category, companyName, picture, publish, function(data,err){
                if(!err){
                    res.send(data);
                }else{

                }
        }); 
    });
    
    }else{
                res.send({'REPLY':'150'});
		//res.redirect('/login');
	}
});


router.post('/editContact', function(req,res){
if(req.session.name){
    var form = new formidable.IncomingForm();
    var contactId, name, phone, email;


    form.parse(req);
    form.on('field', function (field, value) {
        //console.log('fields');
        //console.log(field + " " + value);
         if(field === 'contactId'){
            contactId = value ;
         }else if(field === 'name'){
            name = value;
         }else if(field === 'phone'){
            phone = value;
         } 
         else if(field === 'email'){
            email = value;
         } 
          
    });
    form.on('end', function(){
        Contact.updateContactById(contactId, name, phone, email, function(data, err){
            if(!err){
                res.send(data);
            }
        });
    });

                }else{
                                res.send({'REPLY':'150'});
		//res.redirect('/login');
	}
});

module.exports = router;