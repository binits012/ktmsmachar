var express = require('express');
var router = express.Router();
var Category = require('../model/category');
var User = require('../model/users'); 
var formidable = require('formidable');
 
var NewsContent = require('../model/newsContent'); 
var News =  require('../model/news');
var fs = require('fs');
var ViewCounter = require('../model/viewConter');
var CommentUser = require('../model/commentUser');
var Comment = require('../model/comment');
var request = require('request');
var SendMail = require('../model/sendMail');
var logger = require("../model/logger");
var Advertisement = require('../model/advertisement');
var Writer = require('../model/writer');
/* GET home page. */

router.get('/', function(req, res) {
 // res.render('editorUser', { title: 'News e Nepal' });
});

router.get('/pageCounter', function(req,res){
	ViewCounter.getTotalViewCounterSoFar(function(data, err){
		if (!err) {
			res.send(data);
		}
	});	
});

router.post('/addCategory', function(req,res){
	

	var category = req.body.category;
	var publish =  req.body.publish;
	var index = req.body.index;


	var form = new formidable.IncomingForm();

	form.parse(req);
	form.on('field', function (field, value) {
		////.log('fields');
        //.log(field + " " + value);
         if(field === 'category'){
         	category = value.trim();
         }else if(field === 'publish'){
         	publish = value;
         }else if(field === 'index'){
         	index = value;
         } 
    });

	if(publish === '1'){
		publish = true;
	}else{
		publish = false;
	}
	if(req.session.name){


		form.on('end', function() {
			Category.createCategory(category,publish,index, function(data,err){
				if(!err){
					 logger.info('added category ' + category + ' publish =' + publish + ' index =' + index  ); 
					res.send(data);

				}else{
					logger.info('creation of new category failed %s' , err);
					res.send({'REPLY':'101'});
				}
			});
		});

		

	}else{
		res.send({'REPLY':'150'});
		//res.redirect('/login');
	}
});

router.post('/editCategory', function(req, res){

	var category = req.body.category;
	var publish =  req.body.publish;
	var index = req.body.index;
	var id = req.body.categoryId;

	var form = new formidable.IncomingForm();
	form.parse(req);

	form.on('field', function (field, value) {
		////.log('fields');
        ////.log(field + " " + value);
         if(field === 'category'){
         	category = value.trim();
         }else if(field === 'publish'){
         	publish = value;
         }else if(field === 'index'){
         	index = value;
         }else if(field === 'categoryId'){
         	id = value;
         } 
    });

	if(publish === '1'){
		publish = true;
	}else{
		publish = false;
	}

	if(req.session.name){

		form.on('end', function() {
			Category.editCategory(id,category,publish,index, function(data,err){
			if(!err){
				logger.info('edited category ' + id + 'category='+ category + ' index =' + index);
				////.log('edit category');
				////.log(data);
				res.send(data);

			}else{
				res.send({'REPLY':'101'});
			}
		});
			
		});
		

	}else{
		
		res.send({'REPLY':'150'});
		//res.redirect('/login');
	}

});


router.get('/addNews',function(req,res){
	if(req.session.name){
	var page = req.query.page;
	////.log(page);
	if( typeof(page) === 'undefined'){
		page = 0;

		Category.getAllCategory(function(data,err){
			if(!err){
					News.getRecentNews(page,function(doc,err){
					console.log("RECENT NEWS");
					console.log(doc);
						Writer.getAllActiveWriters(function(writerData, err){
							if(!err){
								logger.info('get news by ' +  req.session.name.name ); 
								res.render('addNews', { title: 'ktmsamachar:: हरपल तपाईकै लागि...', categoryData:data, newsData:doc, writerData:writerData  });
							}	
						})	
						
					});
					
					 
			}else{
				// need to handle the error
			}
		})
	}else{
		News.getRecentNews(page,function(doc,err){
					//.log(doc);
						if(!err){
							 
							res.send(doc);
						}
					});
	}
	
	}else{
		//res.send({'REPLY':'150'});
		res.redirect('/login');
	}
	
});

 
router.post('/addNews',  function(req,res){
	 
	if(req.session.name){ 
	var form = new formidable.IncomingForm();
	//form.uploadDir = "../public/images/uploads"; 
	form.parse(req, function(err, fields, files){
		 
	});
	var category, newsTitle, place, longi, lati, paragraph, paragraph1, paragraph2, paragraph3, paragraph4 ;
	var quotes, quotes1, quotes2, quotes3, quotes4;
	var video, video1, video2, video3, video4;
	var pic, pic1, pic2, pic3, pic4;
	var picName, picName1, picName2, picName3, picName4;
	var breaking, priority;
	var writer, locationName, initialPicture, initialNews,newsWriter,newsWriterImg;

	form.on('field', function (field, value) {
		 if(field === 'category'){
		 	category = value;
		 }else if(field === 'newsTitle'){
		 	newsTitle = value;
		 }else if(field === 'place'){
		 	place = value;
		 }else if(field === 'longi'){
		 	longi = value;
		 }else if(field === 'lati'){
		 	lati = value;
		 }else if(field === 'paragraph'){
		 	paragraph = value;
		 }else if(field === 'paragraph1'){
		 	paragraph1 = value;
		 }else if(field === 'paragraph2'){
		 	paragraph2 = value;
		 }else if(field === 'paragraph3'){
		 	paragraph3 = value;
		 }else if(field === 'paragraph4'){
		 	paragraph4 = value;
		 }else if(field === 'quotes'){
		 	quotes = value;
		 }else if(field === 'quotes1'){
		 	quotes1 = value;
		 }else if(field === 'quotes2'){
		 	quotes2 = value;
		 }else if(field === 'quotes3'){
		 	quotes3 = value;
		 }else if(field === 'quotes4'){
		 	quotes4 = value;
		 }else if(field === 'video'){
		 	video = value;
		 }else if(field === 'video1'){
		 	video1 = value;
		 }else if(field === 'video2'){
		 	video2 = value;
		 }else if(field === 'video3'){
		 	video3 = value;
		 }else if(field === 'video4'){
		 	video4 = value;
		 } else if(field === 'breaking'){
		 	breaking = value;
		 } else if(field === 'priority'){
		 	priority = value;
		 }
		 else if(field === 'writer'){
		 	writer = value;
		 } else if(field === 'locationName'){
		 	locationName = value;
		 }
		 else if(field === 'initialPicture'){
		 	initialPicture = value;
		 }
		 else if(field === 'initialNews'){
		 	initialNews = value;
		 }
		 else if(field === 'newsWriter'){
		 	newsWriter = value;
		 }
		 else if(field === 'newsWriterImg'){
		 	newsWriterImg = value;
		 }
		 ////.log(field);
         
    });

	// image
	form.on('file', function(name, file){
		 ////.log(name + 'file =' + file.name);
		 if(name === 'pic'){
		 	pic = file.path;
		 	picName = file.name;
		 }

	});
    form.on('end', function() {


    	if(pic){
    		fs.readFile(pic, function (err, data) {
    			////.log(picName + 'path' +pic);
    			 var target_path = './public/images/uploads/' + picName;
    			 fs.rename(pic, target_path, function(err){
    			 	if(!err){
    			 		//newsContent, photos, videos, quotes,

    			 		


    			 	}
    			 });
    		});
    	}

    	NewsContent.createNewsContent(paragraph,picName,null,quotes,function(data, err){
    		if(!err){
    			//newsCategory, newsTitle, newsContent, publish, location,
    			//.log('category' + category);
			priority = (priority ==='0') ? false:true;
			breaking = (breaking ==='0') ? false:true;
    			News.createNews(category, newsTitle, data._id, true, null,breaking, priority, writer, locationName, initialPicture, initialNews,
			newsWriter,newsWriterImg,function(doc,err){
    				if(!err){
					logger.info('New news added');
					logger.info(category + 'newsTitle =' + newsTitle );
					logger.info('added news by ' +  req.session.name.name ); 
    					res.redirect('/news/addNews?newsAdded=true');
    				}else{
					logger.info('new news creation error %s', err);
				}
    			});
    			
    		}else{
			
		}
    	});
    	
    	
    });

    }else{
		res.send({'REPLY':'150'});
		//res.redirect('/login');
	}
	 
	//res.send({'REPLY':'OK'});

});


//user edits the newsContent
router.post('/editNews',function(req,res){

	if(req.session.name){
	var form = new formidable.IncomingForm();
	var category, newsTitle,  paragraph, video, newsId;
	var breaking, priority, writer, locationName, initialPicture, initialNews, newsWriter, newsWriterImg;
	//form.uploadDir = "../public/images/uploads"; 
	form.parse(req);
	form.on('field', function (field, value) {
		 if(field === 'category'){
		 	category = value;
		 }else if(field === 'newsTitle'){
		 	newsTitle = value;
		 }else if(field === 'editParagraph'){
		 	paragraph = value;
		 } else if(field === 'video'){
		 	video = value;
		 } else if(field === 'newsId'){
		 	newsId = value;
		 }else if(field === 'breaking'){
		 	breaking = value;
		 }else if(field === 'priority'){
		 	priority = value;
		 }else if(field === 'writer'){
		 	writer = value;
		 }else if(field === 'locationName'){
		 	locationName = value;
		 }else if(field === 'initialPicture'){
		 	initialPicture = value;
		 }
		 else if(field === 'initialNews'){
		 	initialNews = value;
		 }
		 else if(field === 'newsWriter'){
		 	newsWriter = value;
		 }
		 else if(field === 'newsWriterImg'){
		 	newsWriterImg = value;
		 }
		// //.log(field);
         
    });

    form.on('end', function() {

    	////.log('edit parameters');
    	////.log(category,newsTitle, paragraph, video + newsId);
	priority = (priority ==='0') ? false:true;
	breaking = (breaking ==='0') ? false:true;
    	News.editTheSelectedNews(category,newsTitle, paragraph, video ,newsId,breaking, priority,writer,locationName,initialPicture, initialNews,newsWriter,newsWriterImg,function(data,err){
    		 
    		if(data){
    			 
    			res.send({'OK':'OK'});
    		}
    	})
    	
    });
    
    
    }else{
		res.send({'REPLY':'150'});
		//res.redirect('/login');
	}
});


//user clicked on the selected news link

router.get('/:name/:id',function(req,res){

	var today = new Date();
	today = today.toISOString().slice(0,10);
	ViewCounter.getTodayViewCounter(today,function(data,err){
		if(!err){
			////.log('viewcounter'); 
			////.log(data);
			if(data.length == 0){
				
				ViewCounter.createViewCounter(today,1,function(doc,err){
					if(!err){
						////.log('viewcounter created');
						////.log(doc);
					}else{
						////.log('error on viewcoutner creation');
						////.log(err);
					}
				});
			}else{
				var counter = data[0].pageCounter + 1;
				////.log('counter=' + counter);
				////.log(data[0]._id);
				ViewCounter.updateViewCounter(data[0]._id,counter, function(data,err){
					if(!err){
						////.log('data');
						////.log(data);
						 
					}else{
						////.log('err');
						////.log(err);
					}
				});
			}
		}else{
			 
			//.log(err);
		}
	});


	var id = req.params.id;
	// update the newsCounter
	  
	News.updateTheNewsCounter(id,function(data, err){
		if(!err){
			////.log('data');
			////.log(data);

		}else{
			//.log('error');
			////.log(err);
		}
	});
	 
	//ends here

	Category.getAllCategory(function(data,err){
			if(!err){
					//res.render('addNews', { title: 'NewsEnepal', categoryData:data });
				News.getNewsOfSelection(id,function(doc,locationNewsData,writerNewsData, err){
					if(!err){
						if (doc != null && (typeof doc != 'undefined') ) {
							//.log(doc);
							Advertisement.getAdvertisementByCategory(doc[0].newsCategory._id, function(adDoc, err){
							if(!err){
								////.log(adDoc);
								////.log('*****' + id);
								////.log(locationNewsData)
								var newsTitle = 'Newsenepal:: '+doc[0].newsTitle;
								 
								res.render('selectedNews',{title:newsTitle, categoryData:data, newsData:doc,advertData:adDoc, locationNewsData:locationNewsData, writerNewsData:writerNewsData});
							}
							else{
								logger.info(doc);
								logger.error(err);	
							}
							
							}); 
						}
						
						
					}
				});
				
					 
			}else{
				// need to handle the error
			}
		});

});


// get the recentComment
router.get('/comments',function(req,res){
	if(req.session.name){
	 
	Comment.getAllComments(function(data,err){
		if(!err){
			////.log(data);
			res.render('comment',{commentData:data});
		}else{

		}

	});
 
	}else{
		res.redirect('/login');
	}
});

// user click on the category
router.get('/:name', function(req, res){
	var today = new Date();
	today = today.toISOString().slice(0,10);
	ViewCounter.getTodayViewCounter(today,function(data,err){
		if(!err){
			////.log('viewcounter'); 
			////.log(data);
			if(data.length == 0){
				
				ViewCounter.createViewCounter(today,1,function(doc,err){
					if(!err){
						////.log('viewcounter created');
						////.log(doc);
					}else{
						//.log('error on viewcoutner creation');
						//.log(err);
					}
				});
			}else{
				var counter = data[0].pageCounter + 1;
				////.log('counter=' + counter);
				////.log(data[0]._id);
				ViewCounter.updateViewCounter(data[0]._id,counter, function(data,err){
					if(!err){
						////.log('data');
						////.log(data);
						 
					}else{
						//.log('err');
						//.log(err);
					}
				});
			}
		}else{
			 
			////.log(err);
		}
	});

	var name = req.params.name;
	var page = req.query.page;
	////.log(name + "page" + page);
	
	if (typeof(page) === 'undefined' ) {
		Category.getAllCategory(function(data,err){
			if(!err){
				News.getNewsByCategorySelection(name,null,function(doc,err){
					if(!err){
						////.log('undefined is called');
						////.log(doc);
						if (doc) {
							 
							var categoryId = doc[0].newsCategory;
							News.getNewsByCategoryByHigestCounter(name, function(myDoc,err){
								if(!err){
									Advertisement.getAdvertisementByCategory(categoryId, function(adDoc, err){
										if(!err){
											res.render('selectedCategory',{title:'Newseneapl:: हरपल तपाईकै लागि...', categoryData:data, newsData:doc, higestNews:myDoc, advertData:adDoc});
												
										}else{
										
										}	
									});
									
								}	
							}); 
							
						}else{
							res.render('notFound',{title:'Page not found'});
						}
						
					}
				})
			}
		});	 
	}else{
		News.getNewsByCategorySelection(name,page,function(doc,err){
			if(!err){
				////.log('*********category data*******');
				res.send(doc);
			}
		});
	}
	

});

function verifyRecaptchaGotFromCliet(key,callback){
    
  
   
     
 request.post({ 
   url: "https://www.google.com/recaptcha/api/siteverify?secret=6LeOShQUAAAAAMVmsPgj-cuQ6SN0qz9-UfYGLxfm"  + "&response=" + key,
   
 }, function(error, response, body){
   console.log(body);
    var parsedData = JSON.parse(body);
    console.log(parsedData.success);
   if ( parsedData.success ) {
     
      callback(true);
   }else{
     callback(false);
   }
 });  
     
}

//comment user send the comment
router.post('/comment',function(req,res){
	 

	var form = new formidable.IncomingForm();
	var name, email,  comment, newsId, captach;
	//form.uploadDir = "../public/images/uploads"; 
	form.parse(req);
	form.on('field', function (field, value) {
		 if(field === 'name'){
		 	name = value;
		 }else if(field === 'email'){
		 	email = value;
		 }else if(field === 'comment'){
		 	comment = value;
		 } else if(field === 'newsId'){
		 	newsId = value;
		 } else if(field === 'g-recaptcha-response'){
		 	captach = value;
		 }
		 ////.log(field + value);
         
    });

	
    form.on('end', function() {
    	////.log('***********comment********');
	//	//.log(name + email + comment + newsId);
    	//res.send({'REPLY':'110'}); // no comment user is created
 
    	console.log('check user by email address parameters');
	
    	verifyRecaptchaGotFromCliet( captach,function(success){
	
			if(success){
			     // now this makes sure it is not a bot
			console.log('email =' + email);
		    	CommentUser.searchUserByEmail(email,function(data,err){
		    	 	if(!err){
		    	 		if(data===null){
		    	 			// we haven't found the existing comment user, thus create a new comment user using the given email address. 
		    	 			//but first we check whether the given email exits or not
		    	 			var emailCheckUrl = 'http://apilayer.net/api/check?access_key=286277db9fb541507302befc5635a3b6&email='+email+'&smtp=1&format=1'
		    	 			request(emailCheckUrl,function(error, response, body){
		    	 				////.log(response.statusCode);
		    	 				////.log(response);
		    	 				////.log(body);
		    	 				if(!error && response.statusCode == 200){
		    	 					var info = JSON.parse(body);
		    	 					////.log(info);
		    	 					////.log(info.mx_found);
		    	 					if(info.smtp_check ===true){

		    	 						////.log('this is a valid email');

		    	 						CommentUser.createCommentUser(name,email,function(doc,err1){
					    	 				////.log('user data');
					    	 				
					    	 				if(!err1){
					    	 					// new user have been created, now we store the comment into database
					    	 					var userId = doc._id;
					    	 					////.log('userId = '+ userId);

					    	 					Comment.createComment(comment,userId,function(myDoc,err2){
					    	 						if(!err2){
					    	 							//comment is now stored in our system now we need to reference the comment to the respectivew news

					    	 							var commentId = myDoc._id;
					    	 							////.log('commentId' +commentId);
					    	 							News.addNewComment(newsId,commentId,function(myData,err3){
					    	 								if(!err3){
					    	 									res.send(myData);
					    	 									//userName, toEmail, newsTitle, yourComment
					    	 									var sendMail = new SendMail(name,email, myData.newsTitle,comment, null, null);
					    	 									sendMail.sendMail("We acknowledge your comment", "firstComment");
					    	 								}else{
					    	 									res.send({'REPLY':'108'}); //  comment couldn't be stored into the database
					    	 									return ;
					    	 								}
					    	 							});
					    	 						}else{
					    	 							res.send({'REPLY':'109'}); // no comment is stored in the database
					    	 							return;
					    	 						}
					    	 					});

					    	 				}else{
					    	 					// no user is created
					    	 					res.send({'REPLY':'110'}); // no comment user is created
					    	 					return;
					    	 				}
					    	 			});

		    	 					}else{
		    	 						//.log('this is not a valid email');
		    	 						res.send({'REPLY':'107'});
		    	 					}
		    	 					 
		    	 				}else{
		    	 					 // not a valid email address
		    	 				}
		    	 			});
							 
		    	 		}

		    	 		else{
		    	 			//system has found a user

		    	 			var userId = data._id;
		 					////.log('userId = '+ userId);
		 					Comment.createComment(comment,userId,function(myDoc,err2){
		 						if(!err2){
		 							//comment is now stored in our system now we need to reference the comment to the respectivew news

		 							var commentId = myDoc._id;
		 							////.log('commentId' +commentId);

		 							News.addNewComment(newsId,commentId,function(myData,err3){
		 								if(!err3){
		 									res.send(myData);
		 									var sendMail = new SendMail(name, email,myData.newsTitle,comment, null, null);
					    	 				sendMail.sendMail("We acknowledge your comment", "firstComment");
		 								}else{
		 									res.send({'REPLY':'108'}); //  comment couldn't be stored into the database
		 									return ;
		 								}
		 							});
		 						}else{
		 							res.send({'REPLY':'109'}); // no comment is stored in the database
		 							return;
		 						}
		 					});

		    	 		}
		    	 	}
		    	 });   
		    }else{
		    	// there is a problem in captcha validation
		    	res.send({'REPLY':'106'});
		    }
		
	     	
	    });
   	
    });
});


//user liked or disliked the comment
router.post('/likeDislike',function(req,res){

	var form = new formidable.IncomingForm();
	var formType,  commentId, newsId,likeCounter, dislikeCounter;
	form.parse(req);

	form.on('field', function (field, value) {
		 if(field === 'dislikeCounter'){
		 	dislikeCounter = value;
		 }else if(field === 'likeCounter'){
		 	likeCounter = value;
		 }else if(field === 'commentId'){
		 	commentId = value;
		 } else if(field === 'newsId'){
		 	newsId = value;
		 } else if(field === "formType"){
		 	formType = value;
		 }
		// //.log(field + value);
         
    });

	form.on('end', function() {
		////.log('check value');
		////.log(dislikeCounter + 'likeCounter='+ likeCounter +'commentId ='+ commentId + 'newsId ='+ newsId);

		News.updateLikeDislikeCounterOfParticularComment(newsId,commentId,likeCounter,dislikeCounter,function(newsData,data,err){

			if(!err){
				////.log('******** newsData ***********');
				////.log(newsData);
				////.log('******** commentDAta ***********');
				////.log(data);
				res.send(data);
				CommentUser.searchUserById(data.commentUser,function(userDoc,err){
					if(!err){
						var sendMail = new SendMail(userDoc.name, userDoc.email,newsData.newsTitle,data.comment, data.likesCount, data.dislikesCount);

						sendMail.sendMail("Somebody checked your comment!!", "liked");
					}
				});
				 
				 
				
			}else{
				res.send({'REPLY':'ok'});
			}
		});
		

	});

});

router.post('/updatePublishOrHideComment', function(req,res){

if(req.session.name){	
	var form = new formidable.IncomingForm();
	var   commentId, publish;
	form.parse(req);
	form.on('field', function (field, value) {
		 if(field === 'publish'){
		 	publish = value;
		 } else if(field === 'commentId'){
		 	commentId = value;
		 }  
		// //.log(field + value);
         
    });

    form.on('end', function() {
		if(publish === '1'){
			publish = true;
		} else{
			publish = false;
		}
		Comment.updatePublishOrHideComment(commentId,publish,function(userDoc,err){
			if(!err){
				res.send(userDoc);
			}
		}); 

	});


}else{
		res.send({'REPLY':'150'});
		//res.redirect('/login');
	}
});



module.exports = router;