var express = require('express');
var router = express.Router();
var Category = require('../model/category');
var News = require('../model/news');
var ViewCounter = require('../model/viewConter');
var Advertisement = require('../model/advertisement');
/* GET home page. */
router.get('/', function(req, res) {


	//view counter
	var today = new Date();
	today = today.toISOString().slice(0,10);
	ViewCounter.getTodayViewCounter(today,function(data,err){
		if(!err){
			//console.log('viewcounter'); 
			//console.log(data);
			if(data.length == 0){
				
				ViewCounter.createViewCounter(today,1,function(doc,err){
					if(!err){
						//console.log('viewcounter created');
						//console.log(doc);
					}else{
						//console.log('error on viewcoutner creation');
						console.log(err);
					}
				});
			}else{
				var counter = data[0].pageCounter + 1;
				//console.log('counter=' + counter);
				//console.log(data[0]._id);
				ViewCounter.updateViewCounter(data[0]._id,counter, function(data,err){
					if(!err){
						//console.log('data');
						//console.log(data);
						 
					}else{
						//console.log('err');
						//console.log(err);
					}
				});
			}
		}else{
			 
			//console.log(err);
		}
	});

	var breakingNews= null;
	var homeNews, politics, society, bazaar, abroad, games, literature, finance,
	art,photoNews, health,religion, views,abroadViews,videoData, modalData;
	var categoryData;

	var fillEmptyArray = function(name,data,index,dataSize,err){
		if(name==='खेलकुद'){
			games = data;
			 
		}else if(name === 'राजनीति'){
			politics = data;
		}else if(name === 'देश'){
			homeNews = data;
		}else if(name === 'समाज'){
			society = data;
		}
		else if(name === 'विदेश'){
			abroad = data;
		}
		else if(name === 'साहित्य'){
			literature = data;
		}else if (name === 'आर्थिक') {
			finance = data;
		}else if (name === 'कला') {
			art = data
		}else if (name=== 'फोटो खबर') {
			photoNews = data;
		}else if (name === 'स्वास्थ्य') {
			health = data;
		}else if (name === 'धार्मिक') {
			religion = data;
		}else if (name ==='विचार') {
			views = data;
		}else if (name ==='परदेशी आवाज') {
			abroadViews = data;
		}else if (name ==='भिडिओ') {
			videoData = data;
		}else if (name ==='मोडेल') {
			modalData = data;
		} 
		//console.log(index + ' ' + dataSize);
		if(index == dataSize){
			News.getAllNews(function(doc,err){
					//console.log(doc);
					
					News.getPriorityNews(function(pNews, err){
						console.log(pNews)
						if (!err) {
							News.getBreakingNews(function(bNews, err){
								if(!err){
									
									
									News.getPopularNewsOfTheWeek(function(popularData,err){
										if (!err) {
											Advertisement.getAllAdvertisement(1,function(data,err){
												if(!err){
													 
													res.render('index', { title: 'NewseNepal:: हरपल तपाईकै लागि...', categoryData:categoryData, newsData:doc, homeNewsData:homeNews, gamesData:games, politicsData:politics, 
												societyData:society , abroadData:abroad, literatureData:literature, financeData:finance, artData:art, advertData:data, breakingNewsData:bNews,
												priorityNewsData:pNews, photoNewsData:photoNews, healthData:health, religionData:religion, viewsData:views, popularData:popularData,
												abroadViewData:abroadViews, videoData:videoData, modalData:modalData});
												}
											});
										}
									});
									
									
									
								}	
							});
						}
					});
					
					
				});
		}
		
	}
	var index =0;
	Category.getAllCategory(function(data,err){
			if(!err){

				//console.log(data);
				categoryData = data;
				var counter = 0;
				for(var i =0; i< data.length; i++){
					//console.log(data[i].name);
					var name = data[i].name;
					index++;
					(function(name,cb,i){
						//console.log(name);
						 
						News.getFirstTenNewsByCategorySelection(name,function(doc, err){
							counter ++;
						if(!err){
							//console.log('individual data');
							//console.log(doc);
							cb(name,doc, counter,index, null);
						}
					});
					})(name,fillEmptyArray,counter);
					
				}
				
					
					 
			}else{
				// need to handle the error
			}
		})
  
});

/*
router.get('/mobile', function(req, res) {


	//view counter
	var today = new Date();
	today = today.toISOString().slice(0,10);
	ViewCounter.getTodayViewCounter(today,function(data,err){
		if(!err){
			console.log('viewcounter'); 
			console.log(data);
			if(data.length == 0){
				
				ViewCounter.createViewCounter(today,1,function(doc,err){
					if(!err){
						console.log('viewcounter created');
						console.log(doc);
					}else{
						console.log('error on viewcoutner creation');
						console.log(err);
					}
				});
			}else{
				var counter = data[0].pageCounter + 1;
				console.log('counter=' + counter);
				console.log(data[0]._id);
				ViewCounter.updateViewCounter(data[0]._id,counter, function(data,err){
					if(!err){
						console.log('data');
						console.log(data);
						 
					}else{
						console.log('err');
						console.log(err);
					}
				});
			}
		}else{
			 
			console.log(err);
		}
	});

	var breakingNews= null;
	var homeNews, politics, society, bazaar, abroad, games, literature, finance, art;
	var categoryData;

	var fillEmptyArray = function(name,data,index,dataSize,err){
		if(name==='खेलकुद'){
			games = data;
			 
		}else if(name === 'राजनीति'){
			politics = data;
		}else if(name === 'देश'){
			homeNews = data;
		}else if(name === 'समाज'){
			society = data;
		}
		else if(name === 'विदेश'){
			abroad = data;
		}
		else if(name === 'साहित्य'){
			literature = data;
		}else if (name === 'आर्थिक') {
			finance = data;
		}else if (name === 'कला') {
			art = data
		}
		//console.log(index + ' ' + dataSize);
		if(index == dataSize){
			News.getAllNews(function(doc,err){
					//console.log(doc);
					if(!err){
						//res.render('index', { title: 'ktmsamachar:: हरपल तपाईकै लागि...', categoryData:categoryData, newsData:doc, homeNewsData:homeNews, gamesData:games, politicsData:politics, societyData:society , abroadData:abroad, literatureData:literature, financeData:finance, artData:art});
						var tempArray = [ categoryData,  doc,  homeNews,  games,  politics, society ,  abroad,  literature,  finance,  art];
						res.send(tempArray);
					}
				});
		}
		
	}
	var index =0;
	Category.getAllCategory(function(data,err){
			if(!err){

				//console.log(data);
				categoryData = data;
				var counter = 0;
				for(var i =0; i< data.length; i++){
					//console.log(data[i].name);
					var name = data[i].name;
					index++;
					(function(name,cb,i){
						//console.log(name);
						 
						News.getFirstTenNewsByCategorySelection(name,function(doc, err){
							counter ++;
						if(!err){
							//console.log('individual data');
							//console.log(doc);
							cb(name,doc, counter,index, null);
						}
					});
					})(name,fillEmptyArray,counter);
					
				}
				
					
					 
			}else{
				// need to handle the error
			}
		})
  
});
*/

router.get('/member',function(req,res){
	Category.getAllCategory(function(data,err){
		if(!err){
			res.render('member', { title: 'काठमाडौँ समाचार :: हरपल तपाईकै लागि...', categoryData:data});
												 
		}else{
			// need to handle the error
		}
	})
});

router.get('/load', function(req, res){
	
	var page = req.query.page;
	var lower = req.query.lower;
	console.log(page)
	console.log(lower)
	 
		News.getNewsByPage(page,lower,function(data,err){
			//console.log(data)
			res.send(data)
		});
	 
	 
	
	
	
	
});

router.get('/category',function(req, res){
	console.log("category")
	Category.getAllCategory(function(data,err){
		if (err) {
			res.send(null)
		}else{
			res.send(data);
		}
		
	});
});
module.exports = router;
