


var express = require('express');
var router = express.Router();
var Contact = require('../model/contact');
var Category = require('../model/category');
var Advertisement = require('../model/advertisement');
var formidable = require('formidable');
var SendMail = require('../model/sendMail');
var logger = require("../model/logger");

router.get('/',function(req, res){

 if(req.session.name){
	Category.getAllCategory(function(data,err){
			if(!err){
		              
                    Advertisement.getAllAdvertisement(0,function(doc,err){
                        if(!err){
                            res.render('advertisement', { title: 'KTM Samachar', categoryData:data, advertisementData:doc });
                        }else{
                            res.redirect('/login');
                        }
                    });
        			
			}else{
				// need to handle the error
			}
	});
        
        }else{
		res.redirect('/login');
	}
});

router.post('/', function(req,res){

	if(req.session.name){
        var form = new formidable.IncomingForm();
	var category, companyName, picture, startAt, endAt, website, position, contactName, contactPhone, email;


	form.parse(req);

	form.on('field', function (field, value) {
		//console.log('fields');
        //console.log(field + " " + value);
         if(field === 'category'){
         	category = value ;
         }else if(field === 'companyName'){
         	companyName = value;
         }else if(field === 'picture'){
         	picture = value;
         } 
         else if(field === 'startAt'){
         	startAt = value;
         } 
         else if(field === 'endAt'){
         	endAt = value;
         } 
         else if(field === 'website'){
         	website = value;
         } 
         else if(field === 'position'){
         	position = value;
         } 
         else if(field === 'name'){
         	contactName = value;
         } 
         else if(field === 'phone'){
         	contactPhone = value;
         } 
         else if(field === 'email'){
         	email = value;
         } 
    });

    form.on('end',function(){
    	startAt = new Date(startAt);
    	endAt =  new Date(endAt);
    	//console.log(startAt + ' ends at' + endAt);
    	Contact.createContact(contactName,contactPhone,email, function(data,err){
    		if(!err){
    			//console.log('contact front end');
    			//console.log(data);
    			Advertisement.createAdvertisement(companyName, picture, startAt, endAt, position, category, website, data._id, function(doc,err){
    				if(!err){
                                         // advertisement created and send the mail
                                         var sendMail = new SendMail(data.name,data.email,companyName, startAt ,endAt, null, null);
                                         sendMail.sendMail("Advertisement added", "advertisementAdded");
                                         logger.info('advertisement added by  '   );       
    					res.send(doc);
    				}else{

    				}
    			});
    		}else{

    		}
    	});

    });
    
    }else{
                res.send({'REPLY':'150'});
		//res.redirect('/login');
	}


});

router.post('/editAdvert',function(req,res){
                //console.log('******** EDIT ADVERTISEMENT*******');
if(req.session.name){
    var form = new formidable.IncomingForm();
    var category, companyName, picture, startAt, endAt, website, position, advertisementId, publish;


    form.parse(req);

    form.on('field', function (field, value) {
        //console.log('fields');
        //console.log(field + " " + value);
         if(field === 'category'){
            category = value ;
         }else if(field === 'companyName'){
            companyName = value;
         }else if(field === 'picture'){
            picture = value;
         } 
         else if(field === 'startAt'){
            startAt = value;
         } 
         else if(field === 'endAt'){
            endAt = value;
         } 
         else if(field === 'website'){
            website = value;
         } 
         else if(field === 'position'){
            position = value;
         } else if(field === 'advertisementId'){
            advertisementId = value;
         }else if(field === 'publish'){
            publish = value;
         }
          
    });


    form.on('end', function(){
        startAt = new Date(startAt);
        endAt = new Date(endAt);
        if(publish === '1'){
            publish = true;
        }else{
            publish = false;
        }
        Advertisement.updateAdvertisementById(advertisementId, category, companyName, picture, startAt, endAt, website, 
            position, publish, function(data,err){
                if(!err){
                    res.send(data);
                }else{

                }
        });
    });
    
    }else{
                res.send({'REPLY':'150'});
		//res.redirect('/login');
	}
});


router.post('/editContact', function(req,res){
if(req.session.name){
    var form = new formidable.IncomingForm();
    var contactId, name, phone, email;


    form.parse(req);
    form.on('field', function (field, value) {
        //console.log('fields');
        //console.log(field + " " + value);
         if(field === 'contactId'){
            contactId = value ;
         }else if(field === 'name'){
            name = value;
         }else if(field === 'phone'){
            phone = value;
         } 
         else if(field === 'email'){
            email = value;
         } 
          
    });
    form.on('end', function(){
        Contact.updateContactById(contactId, name, phone, email, function(data, err){
            if(!err){
                res.send(data);
            }
        });
    });

                }else{
                                res.send({'REPLY':'150'});
		//res.redirect('/login');
	}
});

module.exports = router;