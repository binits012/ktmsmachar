var express = require('express');
var router = express.Router();
var Role = require('../model/role');
var User = require('../model/users');
var formidable = require('formidable');
/* GET home page. */
router.get('/', function(req, res) {
	if(req.session.name){
		Role.getAllRole(function(data,err){
			User.getAllUsers(function(doc,err){
				if(!err){
					 res.render('superUser', { title: 'SuperUser', roles:data, users:doc });
				}
			});
			
		});
	}else{
		res.redirect('/login');
	}
	
 
});

router.post('/role',function(req,res){
	var roleType = req.body.roleType;

	var form = new formidable.IncomingForm();
	form.parse(req);
	if(req.session.name){

		form.on('field', function (field, value) {
			//console.log('fields');
	        console.log(field + " " + value);
	        if(field === 'roleType'){
	        	roleType = value;
	        }
	         
	    });
	    form.on('end', function() {
	    	 
	    	console.log('*********Role************');
			console.log(roleType);
			if(roleType){
				Role.createRole(roleType,function(data,err){

					if(!err){
						res.send(data);
					}else{
						 
						res.send({'REPLY':'100'});
					}
				});
			}	
	    });


		

	}else{
		res.redirect('/login');
	}
});

router.post('/addUser',function(req,res){
	var form = new formidable.IncomingForm();
	if(req.session.name){
		var userName = req.body.userName;
		var password = req.body.userPassword;
		var role = req.body.roleType;
		var active = req.body.active;
		//console.log(userName + password + role + active);
		 
		var form = new formidable.IncomingForm();
	 
		form.parse(req);
		form.on('field', function (field, value) {
			//console.log('fields');
	        //console.log(field + " " + value);
	         if(field === 'userName'){
	         	username = value;
	         }else if(field === 'userPassword'){
	         	password = value;
	         }else if(field === 'roleType'){
	         	role = value;
	         }else if(field === 'active'){
	         	active = value;
	         }
	    });

	     form.on('end', function() {
	     	//console.log('check role');
	     	//console.log(role);
	    	 if(role){
				User.createUser(username,password,role,active,function(data,err){
						if(!err){
							res.send(data);
						}else{
							if(err.code == 11000){
								res.send({'REPLY':'100'});
							}
							
							}
						});
				}
	   	 });

		
		 
		//res.send({'Reply':'ok'});
	}else{
		res.redirect('/login');
	}
});

module.exports = router;