var express = require('express');
var router = express.Router();
var Role = require('../model/role');
var User = require('../model/users');
var Category = require('../model/category');
var ViewCounter = require('../model/viewConter');
/* GET home page. */
router.get('/', function(req, res) {
	if(req.session.name){
		Category.getAllCategory(function(data,err){
			if(!err){
				
				ViewCounter.getLastSevenDaysViewCounter(function(doc,err){
					if(!err){
						res.render('editorUser', { title: 'काठमाडौँ समाचार :: हरपल तपाईकै लागि...', categoryData:data,user:req.session.name.name, viewCounterData:doc });
					}	
				});
					
			}else{
				// need to handle the error
			}
		})
	
	}else{
		res.redirect('/login');
	}
  
});



module.exports = router;